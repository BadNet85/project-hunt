﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestEventsScript : MonoBehaviour {

    [SerializeField]
    ExternalEventsController controller;


	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnClick()
    {
        controller.AddExternalEvent(ExternalEventType.VillageDestroyed, null, true, new ProjectHunt.GridCoord(-54, 38));
    }
}
