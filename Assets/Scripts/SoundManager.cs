﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class SoundManager : MonoBehaviour
    {
        public static float soundMultiplier = 1f;
        public AudioClip audioFootSteps;
        public AudioClip swampDay, swampNight;
        public AudioClip villageDay, villageNight;
        public AudioClip forestDay, forestNight;
        public AudioClip plainDay, plainNight;
        public AudioClip music;
        public AudioSource audioSteps;
        public AudioSource audioAmbient;
        public AudioSource audioMusic;
        private bool walking;
        private BiomeType currentBiome;
        private DaytimeState lastDaytime;
        Coroutine ambient;
        Coroutine footsteps;
        float currentMusicTime, musicDowntime;
        float lastSoundMultiplier;

        private void OnEnable()
        {
            PlayerGroupIconPin.movementStateChanged += Activatestepsound;
        }

        private void OnDisable()
        {
            PlayerGroupIconPin.movementStateChanged -= Activatestepsound;
        }
        // Use this for initialization
        void Start()
        {
            currentBiome = BiomeType.NaB;
            musicDowntime = 300f;
            audioMusic.volume = 0.1f;
            lastSoundMultiplier = soundMultiplier;
            
        }

        // Update is called once per frame
        void Update()
        {
            if (walking == true && audioSteps.isPlaying == false )
            {
                audioSteps.volume = Random.Range(0.8F, 1);
                audioSteps.pitch = Random.Range(0.8F, 1.1F);
                //audioSteps.PlayOneShot(audioFootSteps);
            }
            currentMusicTime += Time.deltaTime;
            if(currentMusicTime > musicDowntime)
            {
                currentMusicTime = 0f;
                audioMusic.PlayOneShot(music);
            }
            if(lastSoundMultiplier != soundMultiplier)
            {
                if(soundMultiplier < lastSoundMultiplier)
                {
                    StartCoroutine(Fade(audioMusic.volume, audioMusic.volume * soundMultiplier, 2f, audioMusic));
                    StartCoroutine(Fade(audioAmbient.volume, audioAmbient.volume * soundMultiplier, 2f, audioAmbient));
                    StartCoroutine(Fade(audioSteps.volume, audioSteps.volume * soundMultiplier, 2f, audioSteps));
                }
                else if(soundMultiplier > lastSoundMultiplier)
                {
                    StartCoroutine(Fade(audioMusic.volume, soundMultiplier, 2f, audioMusic));
                    StartCoroutine(Fade(audioAmbient.volume, 0.3f * soundMultiplier, 2f, audioAmbient));
                    StartCoroutine(Fade(audioSteps.volume, soundMultiplier, 2f, audioSteps));
                }

                lastSoundMultiplier = soundMultiplier;
            }
        }

        public void Activatestepsound(bool b, BiomeType bT)
        {  
            if(bT != currentBiome || lastDaytime != DayNightCycle.CurrentDaytime)
            {
                currentBiome = bT;
                lastDaytime = DayNightCycle.CurrentDaytime;
                if (ambient != null)
                    StopCoroutine(ambient);

                switch (currentBiome)
                {
                    case BiomeType.NaB:
                        break;
                    case BiomeType.Forest:
                        if(lastDaytime == DaytimeState.Day || lastDaytime == DaytimeState.Dawn)
                            ambient = StartCoroutine(FadeAndSwitchTo(0.3f, 0.5f, audioAmbient, forestDay));
                        else
                            ambient = StartCoroutine(FadeAndSwitchTo(0.3f, 0.5f, audioAmbient, forestNight));
                        break;
                    case BiomeType.Mountain:
                        break;
                    case BiomeType.Plain:
                        if (lastDaytime == DaytimeState.Day || lastDaytime == DaytimeState.Dawn)
                            ambient = StartCoroutine(FadeAndSwitchTo(0.1f, 0.5f, audioAmbient, plainDay));
                        else
                            ambient = StartCoroutine(FadeAndSwitchTo(0.1f, 0.5f, audioAmbient, plainNight));
                        break;
                    case BiomeType.Swamp:
                        if (lastDaytime == DaytimeState.Day || lastDaytime == DaytimeState.Dawn)
                            ambient = StartCoroutine(FadeAndSwitchTo(0.3f, 0.5f, audioAmbient, swampDay));
                        else
                            ambient = StartCoroutine(FadeAndSwitchTo(0.3f, 0.5f, audioAmbient, swampNight));
                        break;
                    case BiomeType.Village:
                        if (lastDaytime == DaytimeState.Day || lastDaytime == DaytimeState.Dawn)
                            ambient = StartCoroutine(FadeAndSwitchTo(0.3f, 0.5f, audioAmbient, villageDay));
                        else
                            ambient = StartCoroutine(FadeAndSwitchTo(0.3f, 0.5f, audioAmbient, villageNight));
                        break;
                    case BiomeType.River:
                        break;
                    default:
                        break;
                }
            }

            if(b == true && b != walking)
            {
                walking = b;
                if (footsteps != null)
                    StopCoroutine(footsteps);
                audioSteps.clip = audioFootSteps;
                audioSteps.volume = 1f;
                audioSteps.Play();
            }
            else if(b == false && b != walking)
            {
                walking = b;
                if(footsteps != null)
                    StopCoroutine(footsteps);
                footsteps = StartCoroutine(Fade(audioSteps.volume, 0.0f, 1f, audioSteps, true));
            }
        }

        public IEnumerator Fade(float start, float end, float duration, AudioSource source, bool stopPlayback = false)
        {
            for (float t = 0F; t<duration; t += Time.deltaTime)
            {
                source.volume = Mathf.Lerp(start, end, t / duration) * soundMultiplier;
                yield return null;
            }
            if (stopPlayback)
            {
                source.Stop();
            }
        }

        public IEnumerator FadeAndSwitchTo(float newSoundVolume, float duration, AudioSource source, AudioClip audioClip)
        {
            for (float t = 0F; t < duration / 2; t += Time.deltaTime)
            {
                source.volume = Mathf.Lerp(source.volume, 0f, t / (duration / 2)) * soundMultiplier;
                yield return null;
            }
            source.clip = audioClip;
            source.Play();
            for (float t = 0; t < duration / 2; t += Time.deltaTime)
            {
                source.volume = Mathf.Lerp(0f, newSoundVolume, t / (duration / 2)) * soundMultiplier;
                yield return null;
            }
        }
    }
}
