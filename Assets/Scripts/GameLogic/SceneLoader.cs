﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ProjectHunt
{
    public class SceneLoader : MonoBehaviour
    {

        private static SceneLoader instance;

        public static SceneLoader Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<SceneLoader>();
                return instance;
            }
        }
        public void LoadScene(string toLoad)
        {
            StartCoroutine(LoadSceneAsync(toLoad));
        }

        public void UnloadScene(string toUnload)
        {
            SceneManager.UnloadSceneAsync(toUnload);
        }

        private IEnumerator LoadSceneAsync(string toLoad)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(toLoad,LoadSceneMode.Additive);

            while(!asyncLoad.isDone)
            {
                yield return null;
            }
        }
    }
}
