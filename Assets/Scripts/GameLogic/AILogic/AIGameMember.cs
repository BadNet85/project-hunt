﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace ProjectHunt
{
    public class AIGameMember : GameMember
    {
        #region Data

        private int doneGroups = 0;
        private Queue<uint> readyQueue;
        private Heatmap heatmap;

        #endregion

        #region Properties

        public Heatmap Heatmap
        {
            get { return heatmap; }
            private set { heatmap = value; }
        }

        #endregion

        // Nötig zum verwenden des events, Verwendbar für setzen des richtigen Groupicons?
        #region Events
        public delegate void AIGroupCreated(Group group);
        public static event AIGroupCreated OnAIGroupCreated;

        protected override void Awake()
        {
            base.Awake();
            heatmap = new Heatmap();
            readyQueue = new Queue<uint>();
        }

        #endregion

        public override Group CreateGroup(GridCubeCoord coord, Entity[] members, int currentAp = -1)
        {
            AIGroup newGroup = ScriptableObject.CreateInstance<AIGroup>();
            if (currentAp == -1)
                newGroup.Init(GenerateGroupID, MemberID, members, coord);
            else
                newGroup.Init(GenerateGroupID, MemberID, members, coord, currentAp);
            groups.Add(newGroup.GroupID, newGroup);
            Debug.Log("Generated Group with ID: " + newGroup.GroupID);
            OnAIGroupCreated?.Invoke(newGroup);
            return newGroup;
        }    

        private void ChangeStrategy()
        {
           
        }

        public void AIGroupDone(uint groupId)
        {
            doneGroups++;
            if(doneGroups < groups.Count && readyQueue.Count > 0)
            {
                (groups[readyQueue.Dequeue()] as AIGroup).ExecuteGroupTurn();
            }
            else
            {
                doneGroups = 0;
                readyQueue.Clear();
                EndTurn();
            }
        }
        public void AIGroupReady(uint groupID)
        {
            readyQueue.Enqueue(groupID);
            if(readyQueue.Count >= groups.Count)
            {
                (groups[readyQueue.Dequeue()] as AIGroup).ExecuteGroupTurn();
            }
        }

        public override void NewTurn()
        {
            foreach (AIGroup group in groups.Values.OfType<AIGroup>())
            {
                group.ResetAP();
                group.PlanGroupTurn();
            }
        }

     
        public void EndTurn()
        {
            //Code that needs to be executed before turn ends
            TurnManager.Instance.EndTurn();
        }

    }


}

