﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class AIGroup : Group
    {
        #region Data

        public delegate void ReadyToMove(Queue<GridTile> path);
        public ReadyToMove rtmDelegate;
        private BehaviourState behaviour;

        #endregion
        
        #region Properties

        public BehaviourState Behaviour
        {
            get
            {
                return behaviour;
            }
            set
            {
                behaviour = value;
            }
        }

        #endregion
   
        #region Methods
        public override void Init(uint id, uint memberID, Entity[] newMembers, GridCubeCoord position, int currentAp = -1)
        { 
            this.GroupID = id;
            this.MemberID = memberID;
            if(newMembers != null)
                for (int i = 0; i < newMembers.Length; i++)
                    members.Add(newMembers[i]);
            RecalculateProperties(); // setzt movementcoeffizienten
            this.Position = position;
            if (currentAp != -1)
                CurrentAP = currentAp;
            Behaviour = new BehaviourStartState(this, PrefabMap.Instance.aiStartBehaviour);
        }

        public override void Init(uint groupID, uint memberID, GridCubeCoord position, int currentAp = -1)
        {
            base.Init(groupID, memberID, position);
            Behaviour = new BehaviourStartState(this, PrefabMap.Instance.aiStartBehaviour);
        }

        protected override void Move(GridCubeCoord newPosition)
        {
            Position = newPosition;
        }

        public void PlanGroupTurn()
        {
            Behaviour.HandleTurn();
        }

        public void ExecuteGroupTurn()
        {
            rtmDelegate(behaviour.Path);
        }

        public void EndGroupTurn()
        {
            //TODO: reevaluate if we want to hold the member in the group as well
            (TurnManager.Instance.GetGameMemberByID(this.MemberID) as AIGameMember)?.AIGroupDone(this.GroupID);
        }
        #endregion
    }
}
