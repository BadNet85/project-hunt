﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class BehaviourInitiateFlee : BehaviourState
    {
        GridCubeCoord destination;

        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if(success && path != null && path.Count > 0)
            {
                for(int i = 0; i < path.Count; i++)
                {
                    if (myGroup.AttemptMove(path[i].Coord, path[i].MovementCost))
                        Path.Enqueue(path[i]);
                    else
                    {
                        Move(path, i + 1);
                        return;
                    }

                    //Arrival Check
                    if (path[i].Coord == (GridCubeCoord)destination)
                    { //Check if forest reached
                        ChangeBehaviour(AIBehaviourType.Flee);
                        return;
                    }
                }
            }

            GridTile gridTile;
            int radius = 0;
            bool validTileFound = false;
            do
            {
                radius++;
                var tiles = GridCubeCoord.GetTilesInRing(myGroup.Position, radius);
                int randomOffset = UnityEngine.Random.Range(0, 5 * radius);
                for(int i = 0; i < tiles.Count; i++)
                {
                    if(radius < 5)
                    {
                        if(MapData.Instance.GetTileAtCoord(tiles[(i + randomOffset) % tiles.Count], out gridTile) && gridTile.IsWalkable && gridTile.biomeType == BiomeType.Forest)
                        {
                            destination = gridTile.Coord;
                            validTileFound = true;
                            break;
                        }
                    }
                    else
                    {
                        while (!MapData.Instance.GetTileAtCoord(tiles[UnityEngine.Random.Range(0, tiles.Count)], out gridTile) || !gridTile.IsWalkable);
                        validTileFound = true;
                        break;
                    }
                }
            } while (!validTileFound);
            Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
            PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.ShortestPath);
        }
    }
}
