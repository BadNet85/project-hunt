﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ProjectHunt;

namespace ProjectHunt
{
    public class BehaviourGoToNest : BehaviourState
    {
        GridCubeCoord destination;
        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if (success && path != null && path.Count > 0)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    //Check if we have AP left. If not move along the path that was created until now.
                    if (myGroup.AttemptMove(path[i].Coord, path[i].MovementCost))
                        Path.Enqueue(path[i]);
                    else
                    {
                        Move();
                        return;
                    }

                    //Arrival Check
                    if (path[i].Coord == destination)
                    {
                        PlayerInRange();
                        //TODO: Bewegen
                        Move();
                        ChangeBehaviour(AIBehaviourType.RandomRoaming);
                        return;
                    }

                    //Player Check
                    if (PlayerInRange())
                    {
                        PathRequestManager.RequestPath(path[i].Coord, destination, CreatePath, AStarMode.WeightedPath);
                        return;
                    }
                }
            }
            destination = MapData.Instance.FindClosestNestCoord(myGroup.Position);
            if(destination == GridCubeCoord.invalid)
            {
                Debug.Log("No Nest found. Returning to: " + AIBehaviourType.RandomRoaming);
                ChangeBehaviour(AIBehaviourType.RandomRoaming);
            }
            Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
            PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.WeightedPath);
        }
    }
}
