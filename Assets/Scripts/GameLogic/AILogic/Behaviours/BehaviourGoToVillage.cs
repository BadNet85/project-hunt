﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ProjectHunt;

namespace ProjectHunt
{
    public class BehaviourGoToVillage : BehaviourState
    {
        private GridCubeCoord destination;

        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if (success && path != null)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    //Check if we have AP left. If not move along the path that was created until now.
                    if (myGroup.AttemptMove(path[i].Coord, path[i].MovementCost))
                        Path.Enqueue(path[i]);
                    else
                    {
                        Move();
                        return;
                    }

                    //Arrival Check
                    if (path[i].Coord == destination)
                    {
                        PlayerInRange();
                        //TODO: Bewegen
                        Move();
                        MapData.Instance.DestroyVillage(destination);
                        ChangeBehaviour(AIBehaviourType.GoToNest);
                        return;
                    }

                    //Player Check
                    if (PlayerInRange())
                    {
                        if (myGroup.AverageHealth > myGroup.AverageMaxHealth / 3)
                        { //HP > 1/3 * MaxHP
                            List<Group> tempGroup;
                            MapData.Instance.GetGroupsAtCoord(GetPlayerInRangePosition(), out tempGroup);
                            if (tempGroup.Count == 1)
                            { //1 group
                                int random = UnityEngine.Random.Range(0, tempGroup[0].Size + 2);
                                if (random == 0)
                                {
                                    ChangeBehaviour(AIBehaviourType.Aggressive);
                                    return;
                                }
                            }
                        }

                        PathRequestManager.RequestPath(path[i].Coord, destination, CreatePath, AStarMode.WeightedPath);
                        return;
                    }
                }
            }

            destination = MapData.Instance.FindClosestVillageCoord(myGroup.Position);
            if (destination == GridCubeCoord.invalid)
            {
                Debug.Log("No Village found. Returning to: " + AIBehaviourType.RandomRoaming);
                ChangeBehaviour(AIBehaviourType.RandomRoaming);
            }
            Path.Enqueue(MapData.Instance.GetTileAtCoord(myGroup.Position));
            PathRequestManager.RequestPath(myGroup.Position, destination, CreatePath, AStarMode.WeightedPath);
        }
    }
}
