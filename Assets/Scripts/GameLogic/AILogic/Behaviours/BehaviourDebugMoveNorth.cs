﻿using System;
using System.Collections.Generic;

namespace ProjectHunt
{
    class BehaviourDebugMoveNorth : BehaviourState
    {
        protected override void CreatePath(List<GridTile> path, bool success)
        {
            if (Path == null)
                Path = new Queue<GridTile>();

            List<GridTile> myPath = new List<GridTile>();

            for (int i = 0; i < 2; i++)
            {
                myPath.Add(MapData.Instance.GetTileAtCoord(myGroup.Position + GridCubeCoord.neighborList[0] * i));
        
            }
            for(int i = 0; i < myPath.Count; i++)
            {
                if (myGroup.AttemptMove(myPath[i].Coord, 0))
                    Path.Enqueue(myPath[i]);
            }
            Move();
        }
    }
}
