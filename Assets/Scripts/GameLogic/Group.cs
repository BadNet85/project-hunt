﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ProjectHunt
{
    public class Group : ScriptableObject
    {
        #region Data
        protected List<Entity> members;
        //public List<ActiveAbility> availableAbilities;
        private int viewDistance = 3;
        private int ap;
        private int maxAP;
        #endregion

        #region Properties
        public int[] MovementCoefficientArray
        {
            get;
            protected set;
        }

        public uint GroupID
        {
            get;
            protected set;
        }

        public uint MemberID
        {
            get;
            protected set;
        }

        public int Size
        {
            get { return members.Count; }
        }
        
        public int MaxAP
        {
            get { return maxAP; }
            private set { maxAP = value; }
        }

        public int MaxAPModification
        {
            get;
            private set;
        }

        public int CurrentAP
        {
            get { return ap; }
            set
            {
                ap = value;
                OnApChanged?.Invoke(CurrentAP);
            }
        }

        public int AverageHealth
        {
            get
            {
                if (members.Count == 0)
                    return 0;
                int hp = 0;
                for (int i = 0; i < members.Count; i++)
                    hp += members[i].HP;
                return hp / members.Count;
            }
        }

        public int AverageMaxHealth
        {
            get
            {
                if (members.Count == 0)
                    return 0;
                int maxhp = 0;
                for (int i = 0; i < members.Count; i++)
                    maxhp += members[i].MaxHP;
                return maxhp / members.Count;
            }
        }

        public int ViewDistance
        {
            get { return viewDistance; }
        }
        [HideInInspector]
        public float TrackReading { get; set; }
        public GridCubeCoord Position { get; protected set; }
        #endregion

        #region Events
        public delegate void APChange(int ap);
        public event APChange OnApChanged;
        public delegate void EntityCountChange(Group group);
        public static event EntityCountChange OnEntitiyCountChange;
 
        private void Awake()
        {
            members = new List<Entity>();
            MovementCoefficientArray = 
                new int[Enum.GetValues(typeof(BiomeType)).Cast<int>().Max()];
            InitializeProperties();
        }

        private void OnEnable()
        {
            DayNightCycle.OnDaytimeChanged += ModifyMaxAPByDaytime;
        }

        private void OnDisable()
        {
            DayNightCycle.OnDaytimeChanged -= ModifyMaxAPByDaytime;
        }
        #endregion

        #region Methods
        public virtual void Init(uint id, uint memberID, Entity[] newMembers, GridCubeCoord position, int currentAp = -1)
        {
            this.GroupID = id;
            this.MemberID = memberID;
            if(newMembers != null)
                for (int i = 0; i < newMembers.Length; i++)
                    members.Add(newMembers[i]);
            RecalculateProperties();
            Position = position;
            if (currentAp != -1)
                CurrentAP = currentAp;
        }

        public virtual void Init(uint groupID, uint memberID, GridCubeCoord position, int currentAp = -1)
        {
            this.GroupID = groupID;
            this.MemberID = memberID;
            Position = position;
            if (currentAp != -1)
                CurrentAP = currentAp;
        }

        public void AddMember(Entity newMember)
        {
            members.Add(newMember);
            newMember.ModifyByPassive(this);
            OnEntitiyCountChange?.Invoke(this);
        }

        public bool RemoveMember(Entity toRemove)
        {
            if (members.Contains(toRemove))
            {
                toRemove.RevertModification(this);
                members.Remove(toRemove);
                OnEntitiyCountChange?.Invoke(this);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get a ReadOnly Container of the group members
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<Entity> GetMembers()
        {
            return members.AsReadOnly();
        }

        protected virtual void Move(GridCubeCoord newPosition)
        {
            var startTile = MapData.Instance.GetTileAtCoord(Position);
            MapData.Instance.DecreaseVisibility(Position, viewDistance);
            MapData.Instance.GetTileAtCoord(Position).OnExit();
            Position = newPosition;
            
            MapData.Instance.ShowTracks(Position);
            MapData.Instance.CheckForNest(Position);
            MapData.Instance.IncreaseVisibility(Position, viewDistance);

            MapData.Instance.GetTileAtCoord(newPosition).OnEnter(this, startTile);
        }

        public bool AttemptMove(GridCubeCoord newPosition, int AP)
        {
            if (CurrentAP >= AP)
            {
                CurrentAP -= AP;
                Move(newPosition);
                return true;
            }
            return false;
        }

        public void ResetAP()
        {
            CurrentAP = MaxAP + MaxAPModification;
        }

        //Initialize the game logic properties to their default values
        private void InitializeProperties()
        {
            for(int i = 0; i < MovementCoefficientArray.Length; i++)
            {
                MovementCoefficientArray[i] = 0;
            }

            MaxAP = 10;
            CurrentAP = 10;
            TrackReading = 0.5f;
        }

        protected void RecalculateProperties()
        {
            for (int i = 0; i < members.Count; i++)
            {
                members[i].ModifyByPassive(this);
            }
        }

        public void ModifyMovementCoefficient(BiomeType type, int mod)
        {
            MovementCoefficientArray[(int)type] += mod;
        }

        public void ModifyMaxAPByDaytime(DaytimeState daytime)
        {
            switch (daytime)
            {
                case DaytimeState.Dawn:
                case DaytimeState.Day:
                case DaytimeState.Dusk:
                    MaxAPModification = 0;
                    break;
                case DaytimeState.Night:
                    MaxAPModification = -(MaxAP / 4);
                    break;
                default:
                    break;
            }
        }

        public void ModifyTrackReading(float mod)
        {
            TrackReading += mod;

            if (TrackReading < 1f)
                TrackReading = 1f;
        }

        public void ModifyAP(int mod)
        {
            int temp = MaxAP + mod;
            MaxAP = temp <= 0 ? 0 : (temp);
        }
        #endregion
    }
}