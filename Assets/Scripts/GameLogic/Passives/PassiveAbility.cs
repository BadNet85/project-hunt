﻿using ProjectHunt;
using UnityEngine;

namespace ProjectHunt
{
    public abstract class PassiveAbility : ScriptableObject
    {
        public string abilityName = "New Passive Ability";

        public abstract void ModifyByPassive(Group group);

        public abstract void RevertModification(Group group);
    }
}