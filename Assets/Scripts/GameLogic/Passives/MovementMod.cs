﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(menuName = "Project Hunt/Logic Elements/Passive Abilities/Movement Modification")]
    public class MovementMod : PassiveAbility
    {
        [SerializeField]
        private BiomeType biomeType;
        [SerializeField]
        private int modification = -1;

        public override void ModifyByPassive(Group group)
        {
            group.ModifyMovementCoefficient(biomeType, modification);
        }

        public override void RevertModification(Group group)
        {
            group.ModifyMovementCoefficient(biomeType, -modification);
        }
    }
}