﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(menuName = "Project Hunt/Logic Elements/Passive Abilities/AP Modification")]
    public class APModification : PassiveAbility
    {
        public int apMod = 1;

        public override void ModifyByPassive(Group group)
        {
            group.ModifyAP(apMod);
        }

        public override void RevertModification(Group group)
        {
            group.ModifyAP(-apMod);
        }
    }
}