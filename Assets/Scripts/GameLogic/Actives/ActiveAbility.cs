﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectHunt;

namespace ProjectHunt
{
    public abstract class ActiveAbility : ScriptableObject
    {

        public string abilityName = "New Active Ability";

        public abstract void TriggerAbility(GridCubeCoord position);
    }
}