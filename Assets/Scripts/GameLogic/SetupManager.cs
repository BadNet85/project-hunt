﻿using UnityEngine;
using ProjectHunt;

public class SetupManager : MonoBehaviour
{
    #region Data
    public static SetupManager Instance
    {
        get;
        private set;
    }

    public static bool IsSingleplayer
    {
        get;
        set;
    }
    #endregion

    #region Events
    public delegate void GameSetup();
    public event GameSetup OnGameSetup;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        IsSingleplayer = true;
        NewGame();
        OnGameSetup?.Invoke();
    }

    #endregion

    #region Methods
    private void NewGame()
    {
        TurnManager.Instance.ClearGameMembers();
        //TODO: Load from Config-File
        GameMember newGameMember = TurnManager.Instance.NewGameMember(true);
        Entity[] hunters = Resources.LoadAll<Entity>("Hunter");
        newGameMember.CreateGroup((GridCoord)PrefabMap.Instance.playerSpawnPoint, hunters);

        if (!IsSingleplayer)
        {
            //TurnManager.Instance.NewGameMember(true);
        }

        newGameMember = TurnManager.Instance.NewGameMember(false);
        Entity[] monsters = Resources.LoadAll<Entity>("Monster");
        newGameMember.CreateGroup((GridCoord)PrefabMap.Instance.monsterSpawnPoint, monsters);
    }
    #endregion
}
