﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace ProjectHunt
{
    public class TurnManager : ScriptableObject
    {
        #region Data
        private Dictionary<uint, GameMember> gameMembers;
        private List<uint> turnSequence;
        private int iterator = 0;
        #endregion

        #region Properties
        //Singleton
        private static TurnManager _instance;
        public static TurnManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<TurnManager>();
                if (_instance == null)
                    _instance = ScriptableObject.CreateInstance<TurnManager>();

                return _instance;
            }
        }
        private static uint generatedMemberID = 0;
        private static uint GenerateMemberID
        {
            get
            {
                return generatedMemberID++;
            }
        }

        public GameMember ActiveGameMember
        {
            get
            {
                GameMember returnMember;
                if(gameMembers.TryGetValue(turnSequence[iterator], out returnMember))
                {
                    return returnMember;
                }
                Debug.Log("No active gameMember found");
                return null;
            }
        }
        #endregion

        #region Events
        private void Awake()
        {
            gameMembers = new Dictionary<uint, GameMember>();
            turnSequence = new List<uint>();
        }

        public delegate void TurnDelegate();
        public static event TurnDelegate OnTurnEnd;
        public static event TurnDelegate OnNewTurn;


        public delegate void EndRoundDelegate();
        public static event EndRoundDelegate OnRoundEnd;
        #endregion

        #region Methods
        public void EndTurn()
        {
            OnTurnEnd?.Invoke();
            iterator++;
            if (iterator >= gameMembers.Count)
            {
                iterator = 0;
                OnRoundEnd?.Invoke();
            }
            Debug.Log("next Turn, current active gameMember: " + iterator);
            gameMembers[turnSequence[iterator]].NewTurn();
            OnNewTurn?.Invoke();
        }

        public GameMember GetGameMemberByID(uint memberID)
        {
            GameMember member;
            if (gameMembers.TryGetValue(memberID, out member))
            {
                return member;
            }
            return null;
        }

        public GameMember NewGameMember(bool controllable)
        {
            GameMember newGameMember;
            if (controllable)
            {
                //newGameMember = new GameMember();
                newGameMember = CreateInstance<GameMember>();
            }
            else
            {
                //newGameMember = new AIGameMember();
                newGameMember = CreateInstance<AIGameMember>();
            }
            newGameMember.Init(GenerateMemberID, "GameMember", controllable);
            gameMembers.Add(newGameMember.MemberID, newGameMember);
            //Todo: sort
            if (controllable)
            {
                turnSequence.Insert(0, newGameMember.MemberID);
            }
            else
            {
                turnSequence.Add(newGameMember.MemberID);
            }
            Debug.Log("Game member with instanced with ID: " + newGameMember.MemberID);
            return newGameMember;
        }

        public bool KillGameMember(GameMember gameMemberToKill)
        {
            if (gameMembers.ContainsKey(gameMemberToKill.MemberID))
            {
                gameMembers.Remove(gameMemberToKill.MemberID);
                turnSequence.Remove(gameMemberToKill.MemberID);
                return true;
            }
            return false;
        }

        public void ClearGameMembers()
        {
            gameMembers.Clear();
            generatedMemberID = 0;
        }
        #endregion
    }
}
