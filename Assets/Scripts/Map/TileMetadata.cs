﻿using System;
using System.Collections.Generic;

namespace ProjectHunt
{
    class TileMetadata
    {
        #region Data

        private List<Group> groupList;

        #endregion

        #region Properties
        public int GroupCount
        {
            get { return groupList.Count; }
        }

        public List<Group> GroupList
        {
            get { return groupList; }
        }
        #endregion

        #region Methods
        public TileMetadata()
        {
            groupList = new List<Group>();
        }

        public Group RemoveGroupById(uint memberID, uint groupID)
        {
            Group tempGroup;
            for (int i = 0; i < groupList.Count; i++)
            {
                if(groupList[i].MemberID == memberID && groupList[i].GroupID == groupID)
                {
                    tempGroup = groupList[i];
                    groupList.RemoveAt(i);
                    return tempGroup;
                }
            }
            return null;
        }

        public void AddGroup(Group group)
        {
            groupList.Add(group);
        }
        #endregion
    }
}
