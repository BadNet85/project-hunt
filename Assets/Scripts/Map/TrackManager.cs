﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ProjectHunt
{

    public class TrackManager
    {
        #region Data
        public class Track
        {
            public GridCubeCoordDirection _direction;
            public ushort _lifetime;
            public ushort _decayPerRound;
            public GameObject icon;
        }
        private Dictionary<GridCubeCoord, List<Track>> monsterTracks;

        public static bool trackUpdateSystemActive = true;

        SimplePool.Pool pool;
        #endregion

        #region Methods
        public TrackManager()
        {
            //pool = new SimplePool.Pool(AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/UI/MonsterTrack/MonsterTrackIcon.prefab"), 20); using UnityEditor
            pool = new SimplePool.Pool(Resources.Load<GameObject>("MonsterTrack/MonsterTrackIcon"), 20);
            monsterTracks = new Dictionary<GridCubeCoord, List<Track>>();
            TurnManager.OnRoundEnd += UpdateMonsterTracks;
        }

        ~TrackManager()
        {
            TurnManager.OnRoundEnd -= UpdateMonsterTracks;
        }

        private void UpdateMonsterTracks()
        {
            if (trackUpdateSystemActive)
            {
                List<GridCubeCoord> keys = monsterTracks.Keys.ToList();
                foreach (GridCubeCoord currentKey in keys)
                {
                    for (int i = 0; i < monsterTracks[currentKey].Count; i++)
                    {
                        monsterTracks[currentKey][i]._lifetime -= monsterTracks[currentKey][i]._decayPerRound;
                        if (monsterTracks[currentKey][i]._lifetime <= 0)
                        {
                            if(monsterTracks[currentKey][i].icon)
                                pool.Despawn(monsterTracks[currentKey][i].icon);
                            monsterTracks[currentKey].Remove(monsterTracks[currentKey][i]);
                        }
                    }
                    if (monsterTracks[currentKey].Count == 0)
                        monsterTracks.Remove(currentKey);
                }
            }
        }

        public void AddTracks(Queue<GridTile> path, ushort lifetime = 5, ushort decayPerRound = 1, ushort randomLifetimeDelta = 2)
        {
            List<GridTile> tiles = path.ToList();
            AddTracks(tiles, lifetime, decayPerRound, randomLifetimeDelta);
        }

        public void AddTracks(List<GridTile> tiles, ushort lifetime = 5, ushort decayPerRound = 1, ushort randomLifetimeDelta = 2)
        {
            for (int i = 0; i < tiles.Count - 1; i++)
            {
                //Add or Subtract the random lifetime delta to the Track
                int tempLifetime = lifetime + ((Random.Range(0, 1) * 2 - 1) * Random.Range(0, randomLifetimeDelta));
                //Create new track struct
                Track track = new Track()
                {
                    _direction = GridCubeCoord.GetNeighbourDirection(tiles[i].Coord, tiles[i + 1].Coord),
                    _lifetime = tempLifetime > 0 ? (ushort)tempLifetime : ushort.MinValue,
                    _decayPerRound = decayPerRound
                };
                //track.icon = pool.Spawn((GridCoord)tiles[i].Coord, Quaternion.Euler(0f, 60f * (int)track._direction, 0f));

                //If monsterTracks already contains the key (tiles[i]), just add the track to the list at key tiles[i]
                if (monsterTracks.ContainsKey(tiles[i].Coord))
                {
                    monsterTracks[tiles[i].Coord].Add(track);
                }
                //If monsterTracks doesn't contain the key(tiles[i]), create a new entry
                else
                {
                    var list = new List<Track> { track };
                    monsterTracks.Add(tiles[i].Coord, list);
                }
            }
        }

        public List<Track> CheckForTrack(GridCubeCoord coord)
        {
            if (monsterTracks.ContainsKey(coord))
            {
                return monsterTracks[coord];
            }
            return null;
        }

        public bool ShowTracks(GridCubeCoord coord)
        {
            if (trackUpdateSystemActive)
            {
                if(monsterTracks.ContainsKey(coord))
                {
                    List<Track> tracks = monsterTracks[coord];
                    for (int i = 0; i < tracks.Count; i++)
                    {
                        //Track track = tracks[i];
                        tracks[i].icon = pool.Spawn((GridCoord)coord, Quaternion.Euler(0f, 60f * (int)tracks[i]._direction, 0f));
                        tracks[i].icon.transform.localPosition += tracks[i].icon.transform.forward * GridCubeCoord.Size;
                    }
                    return true;
                }
            }
            return false;
        }
        #endregion

    }
}
