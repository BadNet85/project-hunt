﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHunt
{
    public class Heatmap : ScriptableObject
    {
        #region Data

        private Dictionary<GridCubeCoord, ValueTuple<int, int>> aiHeatmap;
        private List<GridCubeCoord> alreadySetThisTurn;

        #endregion

        #region Events

        private void Awake()
        {
            alreadySetThisTurn = new List<GridCubeCoord>();
            aiHeatmap = new Dictionary<GridCubeCoord, ValueTuple<int, int>>();
        }

        private void OnEnable()
        {
            TurnManager.OnRoundEnd += ResetSetList;
            TurnManager.OnRoundEnd += UpdateHeatmap;
        }

        private void OnDisable()
        {
            TurnManager.OnRoundEnd -= ResetSetList;
            TurnManager.OnRoundEnd -= UpdateHeatmap;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add weight to the heatmap 
        /// </summary>
        /// <param name="center">Center point of the weighted area</param>
        /// <param name="weight">Weight at the center coordinate</param>
        /// <param name="radius">Radius of the weighted area. Should be less than or equal to weight as every ring around center will be weighted with (weight - radius)</param>
        /// <param name="decayPerRound">Amont of weight reduced each round</param>
        public void AddToHeatmap(GridCubeCoord center, int weight = 10, int radius = 3, int decayPerRound = 1)
        {
            if (alreadySetThisTurn.Contains(center))
                return;
            else
                alreadySetThisTurn.Add(center);

            //Add center to heatmap with full weight and decayPerRound
            if (aiHeatmap.ContainsKey(center))
            {
                aiHeatmap[center] =
                    new ValueTuple<int, int>(
                        Mathf.Clamp(weight + aiHeatmap[center].item1, 0, PrefabMap.Instance.maxHeatmapWeight),
                        decayPerRound + aiHeatmap[center].item2);
            }
            else
            {
                aiHeatmap.Add(center, new ValueTuple<int, int>(weight, decayPerRound));
            }
            for (int currentRadius = 1; currentRadius < radius; currentRadius++)
            {
                //Every ring will have (weight - currentRadius) as it's weight so break if the rings will have 0 or less weight.
                if (weight - currentRadius <= 0)
                    break;
                //Get every GridCubeCoord on ring with current radius i
                List<GridCubeCoord> ring = GridCubeCoord.GetTilesInRing(center, currentRadius);
                for (int j = 0; j < ring.Count; j++)
                {
                    //Add GridCubeCoord with weight and decayPerRound on current ring
                    if (aiHeatmap.ContainsKey(ring[j]))
                    {
                        aiHeatmap[ring[j]] =
                            new ValueTuple<int, int>(
                                Mathf.Clamp(weight + aiHeatmap[ring[j]].item1 - currentRadius, 0, PrefabMap.Instance.maxHeatmapWeight),
                                decayPerRound + aiHeatmap[ring[j]].item2);
                    }
                    else
                    {
                        aiHeatmap.Add(ring[j], new ValueTuple<int, int>(weight - currentRadius, decayPerRound));
                    }
                }
            }
        }

        /// <summary>
        /// Update tile weights in heatmaps
        /// </summary>
        private void UpdateHeatmap()
        { //TODO: Testen!
          //foreach GameMember Dictionary holding the affected tiles and their values
            List<GridCubeCoord> Keys = aiHeatmap.Keys.ToList();
            foreach (var item in Keys)
            {
                if((aiHeatmap[item].item1 - aiHeatmap[item].item2) <= 0)
                {
                    aiHeatmap.Remove(item);
                    //Debug code
                    if (MapData.Instance.IsValidTile(item))
                    {
                        //Debug.Log("Valid gridTile found //Remove");
                        GridTile tile = MapData.Instance.GetTileAtCoord(item);
                        //tile.gameObject.GetComponent<Renderer>().material.color = Color.white;
                    }
                }
                else
                {
                    aiHeatmap[item] = new ValueTuple<int, int>(aiHeatmap[item].item1 - aiHeatmap[item].item2, aiHeatmap[item].item2);
                    //Debug code
                    //MapData.Instance.GetTileAtCoord(item).gameObject.GetComponent<Renderer>().material.color = new Color(0.1f, 0f, 0f) * aiHeatmap[item].item1;
                    if (MapData.Instance.IsValidTile(item))
                    {
                        //Debug.Log("Valid gridTile found //Add");
                        GridTile tile = MapData.Instance.GetTileAtCoord(item);
                        //tile.gameObject.GetComponent<Renderer>().material.color = Color.red;
                    }
                }
            }
        }

        /// <summary>
        /// Returns weight if tile is weighted. Returns 0 if tile is not weighted.
        /// </summary>
        /// <param name="coord">Coordinate of the tile to check</param>
        /// <returns></returns>
        public int TryGetWeight(GridCubeCoord coord)
        {
            ValueTuple<int, int> tuple;
            if (aiHeatmap.TryGetValue(coord, out tuple))
                return tuple.item1;
            return 0;
        }

        /// <summary>
        /// Reset List with the Tiles set this Round. Subscribed to OnEndRound
        /// </summary>
        private void ResetSetList()
        {
            alreadySetThisTurn.Clear();
        }

        #endregion
    }
}