﻿using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class TileSet : ScriptableObject
    {
        #region Data
        public HashSet<GridCubeCoord> set = new HashSet<GridCubeCoord>();
        #endregion
    }
}