﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectHunt;

[CreateAssetMenu(fileName = "PrefabMap", menuName = "Project Hunt/PrefabMap", order = 1)]
public class PrefabMap : SingletonScriptableObject<PrefabMap> {
    #region Data

    public int notPathPenalty;
    public GameObject groupIcon;
    public GameObject monsterIcon;
    public List<GridTileList> prefabGridTileList;
    public int maxHeatmapWeight;
    public Vector3 monsterSpawnPoint;
    public Vector3 playerSpawnPoint;
    public AIBehaviourType aiStartBehaviour;

    #endregion

    #region Methods
    public GridTileList GetListByType(BiomeType type)
    {
        for (int i = 0; i < prefabGridTileList.Count; i++)
        {
            if (prefabGridTileList[i].type == type)
            {
                return prefabGridTileList[i];
            }
        }
        return null;
    }

    public string[] GetVariationsOfType(BiomeType type)
    {
        GridTileList list = GetListByType(type);
        if (list != null)
        {
            string[] array = new string[list.tiles.Count];
            for (int i = 0; i < list.tiles.Count; i++)
            {
                array[i] = list.tiles[i].name;
            }
            return array;
        }
        return null;
    }

    public int GetMovementCostByType(BiomeType biomeType)
    { 
        if (prefabGridTileList.Count - 1 >= (int)biomeType)
        {
            return prefabGridTileList[(int)biomeType].movementCost;
        }

        Debug.Assert(prefabGridTileList.Count >= (int)biomeType, "BiomeType not found for Cost Calculation!");
        return -1;
    }

    public int GetVisionCostByType(BiomeType biomeType)
    {
        if (prefabGridTileList.Count - 1 >= (int)biomeType)
        {
            return prefabGridTileList[(int)biomeType].visionCost;
        }

        Debug.Assert(prefabGridTileList.Count >= (int)biomeType, "BiomeType not found for Vision Calculation!");
        return -1;
    }

    public int GetVisionRangeModifierByType(BiomeType biomeType)
    {
        if (prefabGridTileList.Count - 1 >= (int)biomeType)
        {
            return prefabGridTileList[(int)biomeType].visionRangeModifier;
        }

        Debug.Assert(prefabGridTileList.Count >= (int)biomeType, "BiomeType not found for Vision Range Calculation!");
        return -1;
    }
    #endregion
}

[System.Serializable]
public class GridTileList
{
    public BiomeType type;
    public List<GridTile> tiles = new List<GridTile>();
    public int movementCost;
    public int visionCost;
    public int visionRangeModifier;
}
