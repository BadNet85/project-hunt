﻿using UnityEngine;
using System.Collections;

namespace ProjectHunt
{
    public enum BiomeType
    {
        NaB,
        Forest,
        Mountain,
        Plain,
        Swamp,
        Village,
        River
    }

    [SelectionBase]
    public class GridTile : MonoBehaviour, IHeapItem<GridTile>
    {
        #region Data

        public int tileID = 0;

        public BiomeType biomeType;
        [SerializeField]
        private bool blocked;
        [SerializeField]
        private bool isPath;
        [SerializeField]
        private bool isNest;
        [SerializeField]
        private ScriptableFieldEvent fieldevent;
        [SerializeField]
        private int movementCost = -1;
        [SerializeField]
        private int aiCostModifier = 0;
        [SerializeField]
        private int visionCost = -1;
        [SerializeField]
        private int visionRangeModifier = -1;

        public TextMesh textMesh;

        //Visibility Data
        private int visibility;
        [SerializeField]
        private bool explored;
        private int distance;

        //Pathfinding
        [HideInInspector]
        public int gCost, hCost;
        [HideInInspector]
        public GridTile tileParent;

        #endregion

        #region Events

        private void Awake()
        {
            Prefab = this;
        }

        private void Start()
        {
            if(movementCost == -1)
            {
                movementCost = PrefabMap.Instance.GetMovementCostByType(biomeType);
                if (isPath)
                    movementCost -= 1;
            }
            if (visionCost == -1)
            {
                visionCost = PrefabMap.Instance.GetVisionCostByType(biomeType);
            }
            if (visionRangeModifier == -1)
            {
                visionRangeModifier = PrefabMap.Instance.GetVisionRangeModifierByType(biomeType);
            }
        }
        #endregion

        #region Properties

        public int FCost
        {
            get
            {
                return gCost + hCost;
            }
        }

        public int MovementCost
        {
            get
            {
                return movementCost;
            }
        }

        public int AICostModifier
        {
            get
            {
                return aiCostModifier;
            }
        }

        public GridTile Prefab
        {
            get;
            private set;
        }

        public bool IsPath
        {
            get
            {
                return isPath;
            }
            private set
            {
                isPath = value;
            }
        }

        public bool IsNest
        {
            get
            {
                return isNest;
            }
            private set
            {
                isNest = value;
            }
        }

        public GridCubeCoord Coord
        { //Property, da man GridCoord nicht setzten können sollte! Ergibt sich aus position
            get
            {
                Vector3 position = transform.position;
                GridCubeCoord coord = (GridCoord)position;
                return coord;
            }
        }

        public bool IsWalkable
        {
            get
            {
                return !blocked;
            }
        }

        public int HeapIndex
        {
            get;
            set;
        }

        public ScriptableFieldEvent Fieldevent
        {
            get
            {
                return fieldevent;
            }
            private set
            {
                fieldevent = value;
            }
        }
        public TileShaderData ShaderData
        {
            get;
            set;
        }

        public bool IsVisible
        {
            get
            {
                return (visibility > 0);
            }
        }

        public bool IsExplored
        {
            get
            {
                return explored;
            }
            set
            {
                explored = value;
                ShaderData.RefreshVisibility(this);
            }
        }

        /// <summary>
        /// For Breath Seach
        /// </summary>
        public int Distance
        {
            get;
            set;
        }
        public int VisionModifier
        {
            get
            {
                return visionCost;
            }
        }

        public int VisionRangeModifier
        {
            get
            {
                return visionRangeModifier;
            }
        }
        #endregion

        /// <summary>
        /// Compare Method for Heap
        /// Compares fCost and hCost of two Tiles
        /// </summary>
        /// <param name="TileToCompare"></param>
        /// <returns></returns>
        public int CompareTo(GridTile tileToCompare)
        {
            int compare = FCost.CompareTo(tileToCompare.FCost);
            if (compare == 0)
            {
                compare = hCost.CompareTo(tileToCompare.hCost);
            }
            return -compare;
        }

        public IEnumerator StartFieldEvents(Group group, GridTile startTile)
        {
            if (fieldevent != null && this != startTile && group.GetType() != typeof(AIGroup))
            {
                UIController.Instance.StopIconMovement();
                yield return new WaitUntil(() => UIController.Instance.ActiveGroupIcon.DestinationReached == true);
                fieldevent.OnEnter(group);
            }
        }

        public void OnEnter(Group group, GridTile startTile)
        {
            StartCoroutine(StartFieldEvents(group, startTile));
        }

        public void OnExit()
        {
            if (fieldevent != null)
                fieldevent.OnExit();
        }
        /// <summary>
        /// Set the uv4 coordinates of every vertex of the tile to the tiles coordinates in the TileShaderData texture
        /// </summary>
        /// <param name="sideLength"></param>
        public void InitializeVertexUVs(int sideLength)
        {
            Vector2 textureCoordinate = new Vector2(tileID - Mathf.Floor(tileID / sideLength) * sideLength, Mathf.Floor(tileID / sideLength));
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            Vector2[] uvs = new Vector2[mesh.vertices.Length];
            
            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = textureCoordinate;
            }
            mesh.uv4 = uvs;
        }

        public void IncreaseVisibility()
        {
            visibility++;
            if (visibility == 1)
            {
                IsExplored = true;
                ShaderData.RefreshVisibility(this);
            }
        }

        public void DecreaseVisibility()
        {
            visibility--;
            if (visibility <= 0)
            {
                visibility = 0;
                ShaderData.RefreshVisibility(this);
            }
        }
    }
}