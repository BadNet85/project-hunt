﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
    {

    public enum EventType
    {
        none = 0,
        GroupSplitEvent,
        TextEvent,
        DebuffEvent,
        StoryEvent,
        DescisionEvent,
        setAPEvent,
        buffEvent,
        changeVisionEvent,
        displaceEvent,
        disableRestrictedMovement,
        setRestrictedMovement,
        addHighlightToTile,
        removeHighlight,
        setExplored,
        addCharacterToGroup,
        removeCharacterFromGroup,
        switchTrackManagerState,
        StoryAfterTimeevent,
        modifyAmbientSounds,
    }


    [CreateAssetMenu(fileName = "ScriptableFieldEvent", menuName = "ProjectHunt/ScriptableFieldEvent")]
    public class ScriptableFieldEvent : ScriptableObject, IFieldEvent
    {
        #region ~~VARIABLES~~
        [SerializeField, TextArea]
        private string displayText;
        [SerializeField]
        private string titleText;
        [SerializeField]
        private Sprite flavourImage;
        [SerializeField, Tooltip("Lifetime of the Textbox (0 for infinite / -1 for instant)")]
        private long lifeTime;
        [SerializeField]
        private TextBoxBase textBox;
        [SerializeField]
        private DesicionTextBox desicionTextBox;
        [SerializeField]
        private string storyPart;
        [SerializeField]
        private ScriptableFieldEvent afterStoryEvent;
        [SerializeField]
        private textBoxPosition textPosition;
        [SerializeField]
        private List<EventType> types = new List<EventType>();
        [SerializeField]
        private List<DesicionBase> desicions = new List<DesicionBase>();

        private Group callingGroup;

        //Generic Settings
        [SerializeField]
        private int minGroupSize;
        [SerializeField]
        private bool oneTime;
        [SerializeField]
        private List<Entity> neededEntities = new List<Entity>();
        private Group activatorGroup;
        //Set AP Event
        [SerializeField]
        private int setAP;

        //Buff for days Envent
        [SerializeField]
        private int daysToBuff;
        [SerializeField]
        private int apToBuff;
        private int counter;
        private int originalMaxAP;
        private Group buffedGroup;

        //changeVision Event
        [SerializeField]
        private int radius;
        private List<GridTile> tiles;

        //setRestrictedCoords
        [SerializeField]
        private TileSet restrictedTileset;

        //add/remove highlight
        [SerializeField]
        private Vector3 highlightTile;

        //explore collection of tiles
        [SerializeField]
        private TileSet setExplored;

        //add/remove Character
        [SerializeField]
        private Entity characterToAdd;
        [SerializeField]
        private Entity characterToRemove;

        //Enable/Disable Tracks
        [SerializeField]
        private bool trackManagerActive;

        //Ambient sound
        [SerializeField]
        private float ambientSoundModifier;

        private bool deactivated = false;
        #endregion

        #region ~~PROPERTIES~~
        public string DisplayText
        {
            get
            {
                return displayText;
            }
        }

        public long LifeTime
        {
            get
            {
                return lifeTime;
            }
        }

        public List<EventType> EventTypes
        {
            get
            {
                return this.types;
            }
        }

        public int MinGroupSize
        {
            get
            {
                return this.minGroupSize;
            }
        }

        public bool OneTime
        {
            get
            {
                return this.oneTime;
            }
        }

        public List<Entity> NeededEntities
        {
            get
            {
                return this.neededEntities;
            }
        }

        #endregion


        private bool CheckConditions(Group group)
        {
            //Deaktiviert?
            if (deactivated)
                return false;
            //Gruppengröße ausreichend?
            if (group.Size < minGroupSize)
                return false;
            //einmalig?
            if (oneTime)
                deactivated = true;

            //Sind benötigte Gruppenmitglieder in der Gruppe?
            if (neededEntities.Count >= 1)
            {
                var entities = group.GetMembers();
                int counter = 0;
                int foundEntities = 0;
                foreach (var entity in entities)
                {
                    if (neededEntities[foundEntities].name == entity.name)
                    {
                        foundEntities++;
                    }
                    if (foundEntities == neededEntities.Count)
                        break;
                    counter++;
                    if (counter == entities.Count)
                        return false;
                }
            }
            return true;
        }

        public void OnEnter(Group group)
        {
            callingGroup = group;
            if (!CheckConditions(group))
                return;
            if (types.Contains(EventType.StoryEvent))
            {
                UITextController.Instance.LockSystem(true);
                UITextController.Instance.SetUiActive(false);
                StoryDataCollector.storyPartToShow = storyPart;
                StoryDataCollector.fEvent = this;
                ProjectHunt.SceneLoader.Instance.LoadScene("StoryScene");
            }

            if (types.Contains(EventType.TextEvent))
                UITextController.Instance.QueueTextBox(textPosition, textBox as TextBoxBase, displayText, lifeTime);

            if (types.Contains(EventType.DescisionEvent))
            {
                UITextController.Instance.ShowDesicionBox(desicionTextBox as DesicionTextBox, displayText, lifeTime, titleText, flavourImage, desicions, group);
            }

            if (types.Contains(EventType.GroupSplitEvent))
            {
                TurnManager.Instance.GetGameMemberByID(group.MemberID).SplitGroupHalf(group);
                Debug.Log("SplitGroup");
            }

            if (types.Contains(EventType.DebuffEvent))
                Debug.Log("DebuffGroup");

            if(types.Contains(EventType.setAPEvent))
            {
                group.CurrentAP = setAP;
            }

            if(types.Contains(EventType.buffEvent))
            {
                counter = daysToBuff+1;
                buffedGroup = group;
                originalMaxAP = group.MaxAP;
                group.ModifyAP(apToBuff);
                TurnManager.OnRoundEnd += decayBuff;
            }

            if(types.Contains(EventType.changeVisionEvent))
            {
                tiles = MapData.Instance.GetTilesInRadius(group.Position, radius);
                foreach(GridTile tile in tiles)
                {
                    tile.IncreaseVisibility();
                }
            }

            if(types.Contains(EventType.displaceEvent))
            {
                tiles = MapData.Instance.GetTilesInRadius(group.Position, radius);
                var coords = tiles[(int)Random.Range(0f, tiles.Count)].Coord;
                UIController.Instance.ActiveGroupIcon.MoveImmediate(coords);
                group.AttemptMove(coords, 0);

            }

            if (types.Contains(EventType.disableRestrictedMovement))
            {
                MapData.useRestrictedCoords = false;
            }

            if (types.Contains(EventType.setRestrictedMovement))
            {
                if (restrictedTileset != null)
                {
                    MapData.Instance.SetRestrictedCoords(restrictedTileset.set);
                    MapData.useRestrictedCoords = true;
                }
            }

            if (types.Contains(EventType.addHighlightToTile))
            {
                if(highlightTile != null)
                {
                    ExternalEventsController.Instance.MoveHighlightTo(highlightTile);
                }
            }

            if (types.Contains(EventType.removeHighlight))
            {
                ExternalEventsController.Instance.DisableHighlightMarker();
            }

            if(types.Contains(EventType.StoryAfterTimeevent))
            {
                StoryDataCollector.fEvent = this;
                ExternalEventsController.Instance.InitializeStartAfterTime(storyPart);
            }

            if (types.Contains(EventType.setExplored))
            {
                if(setExplored != null)
                {
                    foreach (var item in setExplored.set)
                    {
                        MapData.Instance.GetTileAtCoord(item).IsExplored = true;
                    }
                }
            }

            if (types.Contains(EventType.addCharacterToGroup))
            {
                if (characterToAdd != null)
                    group.AddMember(characterToAdd);
            }

            if (types.Contains(EventType.removeCharacterFromGroup))
            {
                if (characterToRemove != null)
                    group.RemoveMember(characterToRemove);
            }

            if (types.Contains(EventType.switchTrackManagerState))
            {
                TrackManager.trackUpdateSystemActive = trackManagerActive;
            }

            if (types.Contains(EventType.modifyAmbientSounds))
            {
                SoundManager.soundMultiplier = ambientSoundModifier;
            }
        }

        public void decayBuff()
        {
            counter--;
            if (counter == 0)
            {
                TurnManager.OnRoundEnd -= decayBuff;
                buffedGroup.ModifyAP(-apToBuff);
            }
        }

        public void OnExit()
        {
            if (deactivated)
                return;
            if(types.Contains(EventType.changeVisionEvent))
            {
                foreach(GridTile tile in tiles)
                {
                    tile.DecreaseVisibility();
                }
            }
        }

        public void OnAfterStory()
        {
            if(afterStoryEvent != null)
                afterStoryEvent.OnEnter(callingGroup);
        }
    }
}
