﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ProjectHunt
{
    public class PathRequestManager : MonoBehaviour
    {
        #region Data

        Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
        PathRequest currentPathRequest;

        static PathRequestManager instance;
        Pathfinding pathfinding;

        bool isProcessingPath;

        struct PathRequest
        {
            public GridCubeCoord pathStart;
            public GridCubeCoord pathEnd;
            public AStarMode aStarMode;
            public Action<List<GridTile>, bool> callback;

            public PathRequest(GridCubeCoord _start, GridCubeCoord _end, Action<List<GridTile>, bool> _callback, AStarMode _aStarMode)
            {
                pathStart = _start;
                pathEnd = _end;
                callback = _callback;
                aStarMode = _aStarMode;
            }
        }

        #endregion

        #region Events

        void Awake()
        {
            instance = this;
            pathfinding = GetComponent<Pathfinding>();
        }

        #endregion

        #region Methods

        public static void RequestPath(GridCubeCoord pathStart, GridCubeCoord pathEnd, Action<List<GridTile>, bool> callback, AStarMode aStarMode = AStarMode.ShortestPath)
        {
            PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback, aStarMode);
            instance.pathRequestQueue.Enqueue(newRequest);
            instance.TryProcessNext();
        }

        void TryProcessNext()
        {
            if (!isProcessingPath && pathRequestQueue.Count > 0)
            {
                currentPathRequest = pathRequestQueue.Dequeue();
                isProcessingPath = true;
                pathfinding.StartFindPathAStar(currentPathRequest.pathStart, currentPathRequest.pathEnd, currentPathRequest.aStarMode);
            }
        }

        public void FinishedProcessingPath(List<GridTile> path, bool success)
        {
            currentPathRequest.callback(path, success);
            isProcessingPath = false;
            TryProcessNext();
        }

        #endregion
    }
}