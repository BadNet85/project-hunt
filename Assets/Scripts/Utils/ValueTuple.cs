﻿namespace ProjectHunt
{
    public struct ValueTuple
    {
        public static ValueTuple<T1, T2> Create<T1, T2>(
          T1 item1,
          T2 item2)
        {
            return new ValueTuple<T1, T2>(item1, item2);
        }

        public static ValueTuple<T1, T2, T3> Create<T1, T2, T3>(
          T1 item1,
          T2 item2,
          T3 item3)
        {
            return new ValueTuple<T1, T2, T3>(item1, item2, item3);
        }

        public static ValueTuple<T1, T2, T3, T4> Create<T1, T2, T3, T4>(
          T1 item1,
          T2 item2,
          T3 item3,
          T4 item4)
        {
            return new ValueTuple<T1, T2, T3, T4>(item1, item2, item3, item4);
        }

        public static ValueTuple<T1, T2, T3, T4, T5> Create<T1, T2, T3, T4, T5>(
          T1 item1,
          T2 item2,
          T3 item3,
          T4 item4,
          T5 item5)
        {
            return new ValueTuple<T1, T2, T3, T4, T5>(item1, item2, item3, item4, item5);
        }
    }

    public struct ValueTuple<T1, T2>
    {
        public readonly T1 item1;
        public readonly T2 item2;

        public ValueTuple(
          T1 item1,
          T2 item2)
        {
            this.item1 = item1;
            this.item2 = item2;
        }
    }

    public struct ValueTuple<T1, T2, T3>
    {
        public readonly T1 item1;
        public readonly T2 item2;
        public readonly T3 item3;

        public ValueTuple(
          T1 item1,
          T2 item2,
          T3 item3)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
        }
    }

    public struct ValueTuple<T1, T2, T3, T4>
    {
        public readonly T1 item1;
        public readonly T2 item2;
        public readonly T3 item3;
        public readonly T4 item4;

        public ValueTuple(
          T1 item1,
          T2 item2,
          T3 item3,
          T4 item4)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
        }
    }

    public struct ValueTuple<T1, T2, T3, T4, T5>
    {
        public readonly T1 item1;
        public readonly T2 item2;
        public readonly T3 item3;
        public readonly T4 item4;
        public readonly T5 item5;

        public ValueTuple(
          T1 item1,
          T2 item2,
          T3 item3,
          T4 item4,
          T5 item5)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
            this.item5 = item5;
        }
    }
}
