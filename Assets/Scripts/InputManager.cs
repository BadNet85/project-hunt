﻿using System.Collections;
using System.Collections.Generic;
using ProjectHunt;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    #region Data
    [SerializeField]
    private MapData mapdata;

    private Plane groundPlane;
    private GridTile tile;
    private GridCubeCoord hoverCoord;
    #endregion

    #region Properties
    //public static InputManager Instance
    //{
    //    get;
    //    private set;
    //}
    #endregion

    #region Events
    public delegate void TileEvent(GridTile tile);
    public static event TileEvent OnTileClicked;
    public static event TileEvent OnTileHover;

    public delegate void MouseButtonClicked();
    public static event MouseButtonClicked OnRightMouseClicked;

    private void Awake()
    {
        //Instance = this;
        groundPlane = new Plane(new Vector3(0f, 1f, 0f), Vector3.zero);
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float rayDistance;
        if (EventSystem.current.IsPointerOverGameObject(-1) == false)
        {
            if (groundPlane.Raycast(ray, out rayDistance))
            {
                Vector3 coordVector = ray.GetPoint(rayDistance);
                if (hoverCoord != (GridCubeCoord)coordVector)
                {
                    hoverCoord = (GridCubeCoord)coordVector;
                    if (mapdata.GetTileAtCoord(hoverCoord, out tile))
                        OnTileHover?.Invoke(tile);
                }
                if (Input.GetButtonDown("Fire1") && tile != null)
                {
                    OnTileClicked?.Invoke(tile);
                }
            }
        }

        if(Input.GetButtonDown("Fire2"))
        {
            OnRightMouseClicked?.Invoke();
        }
    }
    #endregion
}
