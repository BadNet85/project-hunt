﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Subtitle", menuName = "ProjectHunt/Subtitle")]
public class Subtitle : ScriptableObject{
    [SerializeField]
    private string chracter;

    [SerializeField, TextArea]
    private string text; 

    public string Character
    {
        get
        {
            return chracter;
        }
    }

    public string Text
    {
        get
        {
            return text;
        }
    }

}
