﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeToBlacktrigger : MonoBehaviour {


    [SerializeField]
    private FadeToBlack fadeToBlack;

    [SerializeField]
    private float duration = 2f;

    private void OnEnable()
    {
       StartCoroutine(fadeToBlack.FadeOut(duration));
    }
}
