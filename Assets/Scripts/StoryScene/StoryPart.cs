﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "StoryPart", menuName = "ProjectHunt/StoryPart")]
    public class StoryPart : ScriptableObject
    {
        public List<string> characters = new List<string>();

        public List<Subtitle> subtitles;

        public PlayableAsset playable;
    }
}
