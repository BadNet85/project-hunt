﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using ProjectHunt;

public class StorySceneController : MonoBehaviour
{

    #region ~~SINGLETON~~
    private static StorySceneController instance;

    public static StorySceneController Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<StorySceneController>();
            return instance;
        }
    }
    #endregion

    [SerializeField]
    private PlayableDirector director;
    [SerializeField]
    private PlayableAsset asses;
    private bool playing = false;
    private bool paused = true;

    [SerializeField]
    private StringCharDictionary characters;

    [SerializeField]
    FadeToBlack fadeToBlack;

    [SerializeField]
    private StringPlayableDictionaryScriptable playables;

    public void Update()
    {
        if (Input.GetButtonDown("Continue"))
        {
            if(playing == true && paused == true)
                director.Play();
        }
    }

    public void Awake()
    {
        StartStorySequence(StoryDataCollector.storyPartToShow);
    }

    public void StartStorySequence(string name)
    {
        foreach(var item in characters.Values)
        {
            item.SetActive(false);
        }

        StoryPart part = null;
        if (playables.Playables.ContainsKey(name))
        {
            part = playables.Playables[name];

            foreach (string character in part.characters)
            {
                this.characters[character].SetActive(true);
            }
        }

        StartPlay(part);
    }

    public IEnumerator EndStorySequence()
    {
        Coroutine fade = StartCoroutine(fadeToBlack.FadeOut(3f));
        yield return new WaitUntil(() => { return fadeToBlack.IsDone; });
        SceneLoader.Instance.UnloadScene("StoryScene");
        UITextController.Instance.SetUiActive(true);
        StoryDataCollector.fEvent.OnAfterStory();
    }

    public void StartPlay(StoryPart part)
    {
        if (playing)
            return;

        playing = true;
        paused = false;
        SubtitleController.Instance.SetSubtitles(part.subtitles);
        director.playableAsset = part.playable;
        SubtitleController.Instance.gameObject.SetActive(false);

        if (director.playableAsset != null)
            director.Play();
    }

    public void AddRufus()
    {
        foreach (var value in characters.Values)
        {
            value.SetActive(false);
        }
        characters["Rufus"].SetActive(true);
        characters["Kamussandra"].SetActive(true);
    }

    public void AddKamu()
    {
        foreach (var value in characters.Values)
        {
            value.SetActive(false);
        }
        characters["Kamu"].SetActive(true);
        characters["Kassandra"].SetActive(true);
    }

    public void Play()
    {
        paused = false;
        director.Play();
    }
    public void Pause()
    {
        paused = true;
        director.Pause();
    }
}
