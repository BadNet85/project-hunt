﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnfadeToBlackTrigger : MonoBehaviour {

    [SerializeField]
    private FadeToBlack fadeToBlack;

    [SerializeField]
    private float duration = 2f;

    private void OnEnable()
    {
        StartCoroutine(fadeToBlack.FadeIn(duration));
    }
}
