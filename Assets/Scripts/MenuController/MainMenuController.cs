﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ProjectHunt;

public class MainMenuController : MonoBehaviour  {

    public void NewGame(bool isSingle)
    {
        SetupManager.IsSingleplayer = isSingle;
        //SceneManager.LoadScene("NewGameMenu");
        LoadingScreen.SceneId = 3;
        SceneManager.LoadScene(1);
    }

    public void EndGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }
    public void UserPreferences()
    {
        SceneManager.LoadScene("UserPref");
    }


}
