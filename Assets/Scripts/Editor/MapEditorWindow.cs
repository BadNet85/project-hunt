﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ProjectHunt;

public class MapEditorWindow : EditorWindow
{

    #region Data
    private static int radius = 30;
    private static bool bGrid = false;
    private static float scale = 1f;

    private static PrefabMap prefabMap;
    private static Transform tileParent;
    private static Vector3[] hexPoints = new Vector3[6];
    private static IList<GridCubeCoord> grid;
    private static BiomeType biomeType;

    public Rect request = new Rect(100, 0, 200, 125);
    //private bool clicked = false;
    #endregion

    //Add Menu Map Editor in Window
    [MenuItem("Window/Map Editor")]
    static void Init()
    {
        MapEditorWindow window = (MapEditorWindow)GetWindow(typeof(MapEditorWindow));
        window.Show();
    }

    private void Awake()
    {
        if (prefabMap == null)
        {
            prefabMap = AssetDatabase.LoadAssetAtPath<PrefabMap>("Assets/ScriptableObjects/PrefabMap.asset");
        }
        if (tileParent == null)
        {
            CreateTilesParent();
        }

        scale = GridCubeCoord.Size * 2;

        //Initialize data for grid drawing
        grid = CreateHexagonalShape(radius);
        hexPoints = PolygonVertices();
    }

    public static void CreateTilesParent()
    {
        Transform[] transforms = Transform.FindObjectsOfType<Transform>();
        bool found = false;
        foreach (var item in transforms)
        {
            if (item.tag == "Tiles")
            {
                tileParent = item;
                found = true;
                break;
            }
        }
        if (!found)
        {
            tileParent = new GameObject("Tiles").transform;
            tileParent.tag = "Tiles";
            tileParent.gameObject.AddComponent<GridVisualizer>();
            Debug.Log("Tiles transform not found! Creating new Tiles transform");
            var tileContainer = FindObjectOfType<MapData>();
            tileContainer.tileContainer = tileParent;
        }
    }

    //Setup editor window
    private void OnGUI()
    {
        GUILayout.Label("Grid Settings", EditorStyles.boldLabel);
        EditorGUI.BeginChangeCheck();
        bGrid = EditorGUILayout.Toggle("Show Grid", bGrid);
        if(EditorGUI.EndChangeCheck())
        {
            CreateTilesParent();
        }
        EditorGUI.BeginChangeCheck();
        radius = EditorGUILayout.IntField("Radius", radius);
        if (EditorGUI.EndChangeCheck())
        {
            if (radius < 1)
                radius = 1;
            else if (radius > 100)
                radius = 100;

            grid = CreateHexagonalShape(radius);
        }

        GUILayout.Space(3);
        GUILayout.Label("Prefill Grid", EditorStyles.boldLabel);
        biomeType = (BiomeType)EditorGUILayout.EnumPopup("Tile to fill Grid with", biomeType);
        if (GUILayout.Button("Fill Grid"))
        {
            FillGrid();
            //clicked = true;
        }

        //TODO: 2-Click-Option for Gridfill

        //if(clicked == true)
        //{
        //    BeginWindows();
        //    request = GUILayout.Window(1, request, RequestWindow, "Warning");
        //    EndWindows();
        //}
    }

    void RequestWindow(int unusedWindowID)
    {
        GUILayout.Label("Make sure that there are no tiles in the Grid!\n" +
            "The script will paste tiles to every field.");
        if (GUILayout.Button("Cancel"))
        {
            //clicked = false;
            return;
        }
        if (GUILayout.Button("Proceed"))
        {
            //clicked = false;
            FillGrid();
        }
    }

    [DrawGizmo(GizmoType.InSelectionHierarchy | GizmoType.NotInSelectionHierarchy)]
    static void DrawGridGizmo(GridVisualizer hexGrid, GizmoType gizmoType)
    {
        if (bGrid && grid != null)
        {
            foreach (GridCubeCoord cubeCoord in grid)
            {
                Vector3 pos = (GridCoord)cubeCoord;      //Zentrum des Feldes durch Cast
                Vector3 first = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
                Vector3 last = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
                bool firstPoint = true;

                for (int i = 0; i < 6; i++)
                {
                    if (firstPoint == true)
                    {
                        first = hexPoints[i] + pos;
                        last = hexPoints[i] + pos;
                        firstPoint = false;
                        continue;
                    }
                    Vector3 next = hexPoints[i] + pos;
                    Gizmos.DrawLine(new Vector3(last.x, 0f, last.z), new Vector3(next.x, 0f, next.z));
                    last = next;
                }

                Gizmos.DrawLine(new Vector3(last.x, 0f, last.z), new Vector3(first.x, 0f, first.z));

            }
        }
    }

    private void FillGrid()
    {
        if (tileParent == null)
        {
            CreateTilesParent();
        }

        foreach (GridCubeCoord cubeCoord in grid)
        {
            Instantiate(prefabMap.GetListByType(biomeType).tiles[0].gameObject,
                (GridCoord)cubeCoord,
                prefabMap.GetListByType(biomeType).tiles[0].transform.rotation,
                tileParent);
        }
    }

    private IList<GridCubeCoord> CreateHexagonalShape(int size)
    {
        var hexes = new List<GridCubeCoord>();

        for (int x = -size; x <= 2 * size + 1; x++)
        {
            for (int z = -size; z <= 2 * size + 1; z++)
            {
                var y = -x - z;
                if (System.Math.Abs(x) <= size && System.Math.Abs(y) <= size && System.Math.Abs(z) <= size)
                {
                    hexes.Add(new GridCubeCoord(x, y, z));
                }
            }
        }
        return hexes;
    }

    private Vector3[] PolygonVertices()
    {
        Vector3[] points = new Vector3[6];

        for (int i = 0; i < 6; i++)
        {
            var angle = 2 * System.Math.PI * (2 * i) / 12;
            points[i] = new Vector3(0.5f * scale * (float)System.Math.Cos(angle), 0f, 0.5f * scale * (float)System.Math.Sin(angle));
        }
        return points;
    }
}
