﻿using UnityEngine;
using UnityEditor;
using ProjectHunt;

[CustomEditor(typeof(ScriptableFieldEvent), true)]
public class EventEditor : Editor {

    SerializedProperty types;
    SerializedProperty lifetime;
    SerializedProperty textBox;
    SerializedProperty desicions;
    SerializedProperty titleText;
    SerializedProperty flavourImage;
    SerializedProperty displayText;
    SerializedProperty textBoxPosistion;
    SerializedProperty minGroupSize;
    SerializedProperty oneTime;
    SerializedProperty neededEntities;
    SerializedProperty desicionTextBox;
    SerializedProperty storyPart;
    SerializedProperty setAP;
    SerializedProperty daysToBuff;
    SerializedProperty apToBuff;
    SerializedProperty radius;
    SerializedProperty restrictedTileset;
    SerializedProperty highlightTile;
    SerializedProperty setExplored;
    SerializedProperty characterToAdd;
    SerializedProperty characterToRemove;
    SerializedProperty trackManagerActive;
    SerializedProperty afterStoryEvent;
    SerializedProperty ambientSoundModifier;

    bool desicionFoldout = false;


    private void OnEnable()
    {
        types = serializedObject.FindProperty("types");
        lifetime = serializedObject.FindProperty("lifeTime");
        textBox = serializedObject.FindProperty("textBox");
        desicions = serializedObject.FindProperty("desicions");
        titleText = serializedObject.FindProperty("titleText");
        flavourImage = serializedObject.FindProperty("flavourImage");
        displayText = serializedObject.FindProperty("displayText");
        textBoxPosistion = serializedObject.FindProperty("textPosition");
        minGroupSize = serializedObject.FindProperty("minGroupSize");
        oneTime = serializedObject.FindProperty("oneTime");
        neededEntities = serializedObject.FindProperty("neededEntities");
        desicionTextBox = serializedObject.FindProperty("desicionTextBox");
        storyPart = serializedObject.FindProperty("storyPart");
        setAP = serializedObject.FindProperty("setAP");
        daysToBuff = serializedObject.FindProperty("daysToBuff");
        apToBuff = serializedObject.FindProperty("apToBuff");
        radius = serializedObject.FindProperty("radius");
        restrictedTileset = serializedObject.FindProperty("restrictedTileset");
        highlightTile = serializedObject.FindProperty("highlightTile");
        setExplored = serializedObject.FindProperty("setExplored");
        characterToAdd = serializedObject.FindProperty("characterToAdd");
        characterToRemove = serializedObject.FindProperty("characterToRemove");
        trackManagerActive = serializedObject.FindProperty("trackManagerActive");
        afterStoryEvent = serializedObject.FindProperty("afterStoryEvent");
        ambientSoundModifier = serializedObject.FindProperty("ambientSoundModifier");
    }


    public override void OnInspectorGUI()
    {
        EditorGUILayout.PropertyField(types, true);
        EditorGUILayout.PropertyField(lifetime);
        EditorGUILayout.PropertyField(minGroupSize);
        EditorGUILayout.PropertyField(oneTime);
        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(neededEntities, true);

        var script = target as IFieldEvent;
        serializedObject.ApplyModifiedProperties();
        if (script.EventTypes.Contains(ProjectHunt.EventType.GroupSplitEvent))
            minGroupSize.intValue = Mathf.Clamp(script.MinGroupSize, 2, 100);

        if(script.EventTypes.Contains(ProjectHunt.EventType.TextEvent))
        {
            EditorGUILayout.LabelField("Text Event Settings",EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(displayText);
            EditorGUILayout.PropertyField(textBoxPosistion);
            EditorGUILayout.PropertyField(textBox);

        }

        if(script.EventTypes.Contains(ProjectHunt.EventType.DescisionEvent))
        {

            EditorGUILayout.Space();
            desicionFoldout = EditorGUILayout.Foldout(desicionFoldout, "Descision Event Settings", true);
            if (desicionFoldout)
            {
                EditorGUILayout.PropertyField(titleText);
                EditorGUILayout.PropertyField(desicionTextBox);
                EditorGUILayout.PropertyField(displayText);
                EditorGUILayout.PropertyField(flavourImage);
                EditorGUILayout.PropertyField(desicions, true);
            }
        }

        if(script.EventTypes.Contains(ProjectHunt.EventType.StoryEvent) || script.EventTypes.Contains(ProjectHunt.EventType.StoryAfterTimeevent))
        {
            EditorGUILayout.LabelField("Story Event Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(storyPart);
            EditorGUILayout.PropertyField(afterStoryEvent);

        }

        if(script.EventTypes.Contains(ProjectHunt.EventType.setAPEvent))
        {
            EditorGUILayout.LabelField("SetAP Event Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(setAP);
        }

        if(script.EventTypes.Contains(ProjectHunt.EventType.buffEvent))
        {
            EditorGUILayout.LabelField("Buff Event settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(daysToBuff);
            EditorGUILayout.PropertyField(apToBuff);

        }
        if(script.EventTypes.Contains(ProjectHunt.EventType.changeVisionEvent))
        {
            EditorGUILayout.LabelField("Change Vision Event Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(radius);
        }

        if(script.EventTypes.Contains(ProjectHunt.EventType.displaceEvent))
        {
            EditorGUILayout.LabelField("Displacement Event Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(radius);
        }

        if (script.EventTypes.Contains(ProjectHunt.EventType.setRestrictedMovement))
        {
            EditorGUILayout.LabelField("Restricted Movement TileSet", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(restrictedTileset);
        }

        if(script.EventTypes.Contains(ProjectHunt.EventType.addHighlightToTile))
        {
            EditorGUILayout.LabelField("Highlight GridTile", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(highlightTile);
        }

        if (script.EventTypes.Contains(ProjectHunt.EventType.setExplored))
        {
            EditorGUILayout.LabelField("Set TileSet as explored", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(setExplored);
        }

        if (script.EventTypes.Contains(ProjectHunt.EventType.addCharacterToGroup))
        {
            EditorGUILayout.LabelField("Character to add", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(characterToAdd);
        }

        if (script.EventTypes.Contains(ProjectHunt.EventType.removeCharacterFromGroup))
        {
            EditorGUILayout.LabelField("Character to remove", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(characterToRemove);
        }

        if (script.EventTypes.Contains(ProjectHunt.EventType.switchTrackManagerState))
        {
            EditorGUILayout.LabelField("Enable/Disable track manager", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(trackManagerActive);
        }

        if (script.EventTypes.Contains(ProjectHunt.EventType.modifyAmbientSounds))
        {
            EditorGUILayout.LabelField("Ambient Sound modifier", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(ambientSoundModifier);
        }

        serializedObject.ApplyModifiedProperties();
    }

}
