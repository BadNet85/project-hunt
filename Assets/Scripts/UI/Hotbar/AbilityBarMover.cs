﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityBarMover : MonoBehaviour {

    private RectTransform rectTransform;

    void Awake()
    {
        rectTransform = this.GetComponent<RectTransform>();
    }

    public void SetInteractable(bool interactable)
    {
        //TODO: Activate Ability Buttons
        StopAllCoroutines();
        StartCoroutine(MoveBar(interactable));
    }

    private IEnumerator MoveBar(bool up)
    {
        Vector2 transVector = rectTransform.position;
        float t = ((rectTransform.position.y) / (-87f));

        do
        {
            t += up ? -0.05f : 0.05f;
            transVector.y = Mathf.Lerp(0f, -87f, t);
            rectTransform.position = transVector;
            yield return new WaitForSeconds(0.01f);
        } while (t < 1f && t > 0f);
    }
}
