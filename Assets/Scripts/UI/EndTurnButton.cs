﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTurnButton : MonoBehaviour {

    private Button button;
    private Image buttonImage;

	void Awake () {
        button = this.GetComponent<Button>();
    }

    public void SetInteractable(bool interactable)
    {
        if (interactable)
        {
            button.interactable = true;
        }
        else
        {
            button.interactable = false;
        }
    }
}
