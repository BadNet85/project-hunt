﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

    #region Data
    private RectTransform myTransform;
    private Vector2 Size;

    Vector3 minScale = new Vector3(0.75f, 0.75f, 0.75f);
    Vector3 maxScale = new Vector3(1.25f, 1.25f, 1.25f);

    //Camera data
    private Transform cameraTransform;
    private float minCamDistance, maxCamDistance;
    private Vector3 flatten = new Vector3(1f, 0f, 1f);
    #endregion

    #region Events
    private void Awake()
    {
        cameraTransform = Camera.main.transform;
        myTransform = GetComponent<RectTransform>();
        Size = myTransform.sizeDelta;
    }
    
    private void Start()
    {
        minCamDistance = Camera.main.transform.parent.GetComponent<CameraController>().MinValue;
        maxCamDistance = Camera.main.transform.parent.GetComponent<CameraController>().MaxValue;
    }

    private void Update ()
    {            
        //Get current Camera position between minCamDistance and maxCamDistance normalized to value between 0 and 1
        float distance = Mathf.Clamp(cameraTransform.position.y, minCamDistance, maxCamDistance);
        float percent = (distance - maxCamDistance) / (minCamDistance - maxCamDistance);

        //Get Rotation direction parallel to Camera looking direction
        Vector3 rotationDirection = Vector3.Scale(cameraTransform.forward, flatten);

        //Rotate icon
        //myTransform.localEulerAngles = new Vector3(60f * (1 - percent), 0f, 0f);
        myTransform.LookAt(myTransform.position + rotationDirection);
        myTransform.localEulerAngles = new Vector3(60f * (1 - percent), myTransform.localEulerAngles.y, myTransform.localEulerAngles.z);

        //Scale icon
        myTransform.sizeDelta = new Vector2(Size.x, Size.y * ((1 + percent) * Mathf.Pow(percent, 2) * 0.5f));
        myTransform.localScale = Vector3.Lerp(maxScale, minScale, percent);
    }
    #endregion
}
