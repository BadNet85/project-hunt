﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESCMenu : MonoBehaviour {
    private enum MenuState {Menu, Quit};
    private MenuState state;
    #region Methods

    public void QuitApplication()
    {
        state = MenuState.Quit;
    }

    public void MainMenu()
    {
        state = MenuState.Menu;
    }


    public void AreYourSure()
    {
        if (state == MenuState.Quit)
        {
            Application.Quit();
        }
        else if(state == MenuState.Menu)
        {
            LoadMainMenu();
        }
    }

    private void LoadMainMenu()
    {
        LoadingScreen.SceneId = 0;
        SceneManager.LoadSceneAsync(1);
    }
    #endregion
}
