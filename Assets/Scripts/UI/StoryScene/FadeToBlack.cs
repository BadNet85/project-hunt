﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour {

    [SerializeField]
    private Image black;

    bool isDone = false;

    public bool IsDone
    {
        get
        {
            return isDone;
        }
    }

    public IEnumerator FadeIn(float duration)
    {
        isDone = false;
        black.CrossFadeAlpha(0, duration, false);
        yield return new WaitForSeconds(duration);
        isDone = true;
    }


    public IEnumerator FadeOut(float duration)
    {
        isDone = false;
        black.CrossFadeAlpha(1, duration, false);
        yield return new WaitForSeconds(duration);
        isDone = true;
    }
}
