﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneTrigger : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(StorySceneController.Instance.EndStorySequence());
    }
}
