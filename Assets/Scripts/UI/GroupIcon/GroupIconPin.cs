﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ProjectHunt
{
    public abstract class GroupIconPin : MonoBehaviour
    {
        #region Data
        protected Group group;

        [SerializeField]
        protected GroupIconHead head;
        protected RectTransform pinRectTransform;
        private RectTransform headRectTransform;
        private Vector2 pinSize;

        [SerializeField]
        protected Image image;
        protected Color color;

        Vector3 minScale = new Vector3(0.75f, 0.75f, 0.75f);
        Vector3 maxScale = new Vector3(1.25f, 1.25f, 1.25f);
         
        [Range(0f, 10f)]
        public float movementSpeed = 2f;
        protected Queue<GridTile> movementPath;

        private float minCamDistance, maxCamDistance;
        private Transform cameraTransform;
        private Vector3 flatten= new Vector3(1f, 0f, 1f);
        #endregion

        #region Properties
        public uint GroupID
        {
            get
            {
                return group.GroupID;
            }
        }
        public uint MemberID
        {
            get
            {
                return group.MemberID;
            }
        }
        public bool DestinationReached
        {
            get;
            protected set;
        }
        #endregion

        #region Events
        protected void Awake()
        {
            cameraTransform = Camera.main.transform;
            pinRectTransform = GetComponent<RectTransform>();
            headRectTransform = head.GetComponent<RectTransform>();
            pinSize = pinRectTransform.sizeDelta;
            image = GetComponent<Image>();
            DestinationReached = true;
            color = GetComponentInChildren<Image>().color;
        }

        private void Start()
        {
            minCamDistance = Camera.main.transform.parent.GetComponent<CameraController>().MinValue;
            maxCamDistance = Camera.main.transform.parent.GetComponent<CameraController>().MaxValue;
            //Debug.Log("Min Cam Distance: " + minCamDistance + ", " + maxCamDistance);
        }

        protected void Update()
        {
            //Get current Camera position between minCamDistance and maxCamDistance normalized to value between 0 and 1
            float distance = Mathf.Clamp(cameraTransform.position.y, minCamDistance, maxCamDistance);
            float percent = (distance - maxCamDistance) / (minCamDistance - maxCamDistance);

            //Get Rotation direction parallel to Camera looking direction
            Vector3 rotationDirection = Vector3.Scale(cameraTransform.forward, flatten);

            //Rotate Head
            headRectTransform.localEulerAngles = new Vector3(60f * (1 - percent), 0f, 0f);

            //Rotate Pin
            pinRectTransform.LookAt(pinRectTransform.position + rotationDirection);

            //Scale Pin and Head
            pinRectTransform.sizeDelta = new Vector2(pinSize.x, pinSize.y * ((1 + percent) * Mathf.Pow(percent, 2) * 0.5f));
            pinRectTransform.localScale = Vector3.Lerp(maxScale, minScale, percent);
            
        }
        #endregion

        #region Methods
        public virtual void Init(Group group, UIController uiController)
        {
            this.group = group;
            head.Init(uiController, group);
            MoveImmediate(group.Position);
        }

        public abstract void StartMovement(Queue<GridTile> path);

        protected abstract IEnumerator Move();

        public void SetColor(Color color)
        {
            this.color = color;
            image.color = color;
            head.SetColor(color);
        }

        public void MoveImmediate(GridCubeCoord coord)
        {
            pinRectTransform.position = (GridCoord)coord;
        }
        #endregion
    }
}