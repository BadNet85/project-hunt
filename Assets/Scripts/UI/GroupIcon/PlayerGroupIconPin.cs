﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    class PlayerGroupIconPin : GroupIconPin
    {
        #region Data
        private bool stopMovement = false;
        #endregion

        #region Events
        public delegate void PlayerIconStoppedMoving();
        public static event PlayerIconStoppedMoving OnPlayerIconStoppedMoving;
        public delegate void MovementStateChanged(bool moving, BiomeType type);
        public static MovementStateChanged movementStateChanged;

        private void OnEnable()
        {
            InputManager.OnRightMouseClicked += StopMovement;
        }

        private void OnDisable()
        {
            InputManager.OnRightMouseClicked -= StopMovement;
            Group.OnEntitiyCountChange -= EntityCountChanged;
        }
        #endregion

        #region Methods
        public override void Init(Group group, UIController uiController)
        {
            base.Init(group, uiController);
            Group.OnEntitiyCountChange += EntityCountChanged;
            EntityCountChanged(group);
        }

        public void SetHighlight(bool highlighted)
        {
            (head as PlayerGroupIconHead)?.SetHighlight(highlighted);
        }

        private void EntityCountChanged(Group group)
        {
            if(group.GroupID == GroupID && group.MemberID == MemberID)
                (head as PlayerGroupIconHead)?.SetEntityCount(group.Size);
        }

        /// <summary>
        /// Sets flag to stop the movement at the next reached tile.
        /// </summary>
        public void StopMovement()
        {
            stopMovement = true;
            MapData.Instance.UpdateGroupPositions();
        }

        public void SetInteractable(bool interactable)
        {
            if (interactable)
            {
                image.color = color;
            }
            else
            {
                image.color = Color.white;
            }
            (head as PlayerGroupIconHead)?.SetInteractable(interactable);
        }

        /// <summary>
        /// Starts icon movement along the given path.
        /// </summary>
        /// <param name="path"></param>
        public override void StartMovement(Queue<GridTile> path)
        {
            DestinationReached = false;
            movementPath = path;
            StartCoroutine(Move());
        }

        protected override IEnumerator Move()
        {
            List<Group> groups;
            GridTile tile;
            //movementStateChanged?.Invoke(true, MapData.Instance.GetTileAtCoord(group.Position).biomeType);
            while (movementPath.Count > 0 && !stopMovement)
            {
                tile = movementPath.Dequeue();

                //Check if Group has enough AP
                if (!(group.AttemptMove(tile.Coord, tile.MovementCost)))
                    break;
                movementStateChanged?.Invoke(true, MapData.Instance.GetTileAtCoord(group.Position).biomeType);

                while (Vector3.Distance(transform.position, (GridCoord)tile.Coord) > 0.01f)
                {
                    float step = movementSpeed * Time.deltaTime;
                    pinRectTransform.position = Vector3.MoveTowards(transform.position, (GridCoord)tile.Coord, step);
                    yield return null;
                }
                tile.gameObject.layer = 0;

                //Check for enemy groups at position and start fight accordingly
                if(MapData.Instance.GetGroupsAtCoord(group.Position, out groups, (int)group.MemberID) && groups.Count > 0)
                {
                    stopMovement = true;
                    Battle.Instance.SetupBattle(group, groups[0] as AIGroup);
                }
            }
            stopMovement = false;
            DestinationReached = true;
            movementStateChanged?.Invoke(false, MapData.Instance.GetTileAtCoord(group.Position).biomeType);
            OnPlayerIconStoppedMoving?.Invoke();
        }
        #endregion
    }
}
