﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ProjectHunt
{
    public class AIGroupIconPin : GroupIconPin
    {
        #region Data
        private bool visible = true;
        #endregion

        #region Events
        private void OnEnable()
        {
            MapData.OnVisibilityChanged += CheckVisibility;
        }

        private void OnDisable()
        {
            MapData.OnVisibilityChanged -= CheckVisibility;
        }
        #endregion

        #region Methods
        public override void Init(Group group, UIController uiController)
        {
            base.Init(group, uiController);
            (group as AIGroup).rtmDelegate = StartMovement;
            CheckVisibility();
        }

        public override void StartMovement(Queue<GridTile> path)
        {
            //Adding tracks to MapData
            MapData.Instance.AddTracks(path);

            DestinationReached = false;
            movementPath = path;
            //Dequeueing the position the group is standing on
            movementPath.Dequeue();
            StartCoroutine(Move());
        }

        protected override IEnumerator Move()
        {
            GridTile tile;
            while (movementPath.Count > 0)
            {
                tile = movementPath.Dequeue();

                if (!tile.IsVisible && !visible)
                {
                    MoveImmediate(tile.Coord);
                    continue;
                }

                if (tile.IsVisible && !visible)
                    yield return new WaitForSeconds(1f);

                Vector3 startPosition = transform.position;
                float step = 0;
                while (step <= 1)
                {
                    step += (movementSpeed * Time.deltaTime) / 2;
                    pinRectTransform.position = Vector3.Lerp(startPosition, (GridCoord)tile.Coord, step);

                    if (tile.IsVisible && !visible)
                    {
                        color.a = Mathf.Clamp01((step * 2f) - 0.5f);
                        SetColor(color);
                    }
                    else if (!tile.IsVisible && visible)
                    {
                        color.a = Mathf.Clamp01(1 - (step * 2f) - 0.5f);
                        SetColor(color);
                    }

                    yield return null;
                }
                visible = tile.IsVisible ? true : false;
            }
            List<Group> groups;
            //Check for enemy groups at position and start fight accordingly
            if (MapData.Instance.GetGroupsAtCoord(group.Position, out groups, (int)group.MemberID))
            {
                Battle.Instance.SetupBattle(groups[0] as Group, group as AIGroup);
            }
            DestinationReached = true;

            //TODO: reevaluate if we want to end the turn through the group or gamemember
            (group as AIGroup).EndGroupTurn();
        }

        private void CheckVisibility()
        {
            GridTile tile = MapData.Instance.GetTileAtCoord((GridCoord)pinRectTransform.position);
            if (tile.IsVisible)
            {
                if (visible)
                    return;
                visible = true;
                StopCoroutine("Fade");
                StartCoroutine(Fade(true));
            }
            else
            {
                if (!visible)
                    return;
                visible = false;
                StopCoroutine("Fade");
                StartCoroutine(Fade(false));
            }
        }

        private IEnumerator Fade(bool fadeIn)
        {
            float step = 0;
            while (step <= 1)
            {
                step += (movementSpeed * Time.deltaTime) / 2;
                if (fadeIn)
                {
                    color.a = Mathf.Clamp01(step);
                    SetColor(color);
                }
                else
                {
                    color.a = Mathf.Clamp01(1 - step);
                    SetColor(color);
                }
                yield return null;
            }
        }
        #endregion
    }
}
