﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour {

    [SerializeField]
    private Text progress;
    [SerializeField]
    private AsyncOperation ao;
    [SerializeField]
    private Slider progressSlider;
    [SerializeField]
    private Image image;
    [SerializeField]
    private Sprite[] sprites;
    [SerializeField]
    private float imageInterval;
    private bool loading;
    private float time;
    private enum FadeState { NoFade, FadeIn, FadeOut};
    private FadeState fadeState;
    private FadeState lastFadeState;
    private float fade;
    private float startTime;
    [SerializeField]
    private float fadeDuration = 1f;
    private int lastIndex;
    private int random;
    public static int SceneId
    {
        get;
        set;
    }

	// Use this for initialization
	void Start () {
        loading = false;
        //TODO: This is only for debugging! Call LoadingScreen.SceneId from the main menu!
        //SceneId = 2;
        do
        {
            random = Random.Range(0, sprites.Length);
        } while (lastIndex == random);
        lastIndex = random;
        image.sprite = sprites[random];
        fadeState = FadeState.FadeIn;
        Random.InitState(System.DateTime.Now.Millisecond);
        Debug.Log("SceneID:" + SceneId);
    }
	
	// Update is called once per frame
	void Update () {
        if (!loading)
        {
            /*
            if (SceneManager.GetSceneByBuildIndex(SceneId).IsValid())
            {
                StartCoroutine(LoadScene());
            }
            else
            {
                Debug.Log("Loading Scene failed. Scene with ID: " + SceneId + " not found!");
            }
            */
            loading = true;
            StartCoroutine(LoadScene());
        }

        time += Time.deltaTime;
        float t = (Time.time - startTime) / fadeDuration;
        switch (fadeState)
        {
            case FadeState.NoFade:
                if (lastFadeState == FadeState.FadeIn && time > imageInterval)
                {
                    fadeState = FadeState.FadeOut;
                    startTime = Time.time;
                }
                else if (lastFadeState == FadeState.FadeOut)
                {
                    do
                    {
                        random = Random.Range(0, sprites.Length);
                    } while (lastIndex == random);
                    lastIndex = random;
                    image.sprite = sprites[random];
                    fadeState = FadeState.FadeIn;
                    startTime = Time.time;
                }
                break;
            case FadeState.FadeIn:
                if(image.color.a < 1f)
                {
                    image.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(0f, 1f, t));
                }
                else
                {
                    lastFadeState = FadeState.FadeIn;
                    fadeState = FadeState.NoFade;
                    time = 0f;
                }
                break;
            case FadeState.FadeOut:
                if (image.color.a > 0f)
                {
                    image.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(1f, 0f, t));
                }
                else
                {
                    lastFadeState = FadeState.FadeOut;
                    fadeState = FadeState.NoFade;
                    time = 0f;
                }
                break;
            default:
                break;
        }
    }

    IEnumerator LoadScene()
    {
        ao = SceneManager.LoadSceneAsync(SceneId);
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            yield return new WaitForSeconds(0.05f);
            progress.text = "Loading... " + ao.progress.ToString();
            progress.color = new Color(progress.color.r, progress.color.g, progress.color.b, Mathf.PingPong(Time.time, 1));
            progressSlider.value = ao.progress;

            if (ao.progress >= 0.9f)
            {
                progressSlider.value = 1f;
                progress.text = "Loading done!";
                if (Input.anyKey)
                {
                    ao.allowSceneActivation = true;
                }
            }
            yield return null;
        }

    }
}
