﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using ProjectHunt;

public class ExternalEventsController : MonoBehaviour {

    #region ~~SINGLETON~~
    private static ExternalEventsController instance;

    public static ExternalEventsController Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<ExternalEventsController>();
            return instance;
        }
    }
    #endregion

    private List<ExternalEvent> externalEvents = new List<ExternalEvent>();
    [SerializeField]
    private GameObject eventButtonPrefab;

    [SerializeField]
    private TextBoxBase defaultTextBoxPrefab;
    private EventButton eventButton;
    [SerializeField]
    private RectTransform UICanvas;
    private ExternalEventTextBox activeTextBox;
    private TextBoxBase toFadeOut;
    private bool eventButtonActive = false;

    [SerializeField]
    private GameObject highlightMarker;

    private void Update()
    {
        if(externalEvents.Count >= 1 && !eventButtonActive)
        {
            eventButton = Instantiate(eventButtonPrefab, UICanvas).GetComponent<EventButton>();
            eventButtonActive = true;
        }
        if (externalEvents.Count >= 1 && eventButtonActive)
        {
            eventButton.UpdateCounter(externalEvents.Count);
        }
        if(externalEvents.Count < 1 && eventButtonActive)
        {
            Destroy(eventButton.gameObject);
            eventButtonActive = false;
        }
        if(activeTextBox != null && externalEvents.Count >= 1)
        {
            eventButton.AddFadeOut();
        }
        else if(activeTextBox == null && externalEvents.Count >= 1)
        {
            eventButton.RemoveFadeOut();
        }
    }

    public void AddExternalEvent(ExternalEvent externalEvent)
    {
        externalEvents.Add(externalEvent);
    }

    public void AddExternalEvent(ExternalEventType type, TextBoxBase prefab, bool fade, GridCubeCoord coord)
    {
        if (prefab == null)
            prefab = defaultTextBoxPrefab;
        ExternalEvent extEvent = new ExternalEvent(type, prefab, fade, coord);
        
        externalEvents.Add(extEvent);
    }

    public void ShowExternalEvent()
    {
        if (activeTextBox != null)
            toFadeOut = activeTextBox;
        if (externalEvents.Count - 1 >= 0)
        {
            activeTextBox = UITextController.Instance.ShowExternalEventBox(externalEvents[externalEvents.Count - 1]);
            activeTextBox.ExternalEvent = externalEvents[externalEvents.Count - 1];
            externalEvents.RemoveAt(externalEvents.Count - 1);
            activeTextBox.Init();
        }
    }

    public void HideExternalEvent()
    {
        UITextController.Instance.HideExternalEventTextBox(toFadeOut);
    }

    public void ForceHideExternalEvent(TextBoxBase textBox)
    {
        UITextController.Instance.HideExternalEventTextBox(textBox);
    }

    public void MoveHighlightTo(Vector3 coord)
    {
        if (highlightMarker != null)
            highlightMarker.transform.position = coord;
        if (!highlightMarker.activeSelf)
            highlightMarker.SetActive(true);
    }

    public void DisableHighlightMarker()
    {
        if (highlightMarker != null)
            highlightMarker.SetActive(false);
    }

    public void InitializeStartAfterTime(string name)
    {
        StartCoroutine(StartEventAfterTime(name));
    }

    public IEnumerator StartEventAfterTime(string name)
    {
        DayNightCycle.Instance.AdvanceDaytimeTo(DaytimeState.Day);
        UIController.Instance.Raycaster.enabled = false;
        yield return new WaitUntil(() => { return DayNightCycle.Instance.cycleDone == true; });
        StoryDataCollector.storyPartToShow = name;
        UITextController.Instance.SetUiActive(false);
        SceneLoader.Instance.LoadScene("StoryScene");
        UIController.Instance.Raycaster.enabled = true;
    }
}
