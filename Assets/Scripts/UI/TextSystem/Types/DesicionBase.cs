﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "New Decision", menuName = "ProjectHunt/New Desicion")]
    public class DesicionBase : ScriptableObject, IDesicion
    {
        [SerializeField]
        private string text;
        [SerializeField]
        private List<ScriptableFieldEvent> events;

        private Group callGroup;

        public string Text
        {
            get
            {
                return text;
            }

            set
            {
                text = value;
            }
        }

        public Group CallGroup
        {
            get
            {
                return callGroup;
            }
            set
            {
                callGroup = value;
            }
        }

        public virtual void HandleDesicion()
        {
            foreach (IFieldEvent _event in events)
            {
                _event.OnEnter(callGroup);
            }
        }
    }
}