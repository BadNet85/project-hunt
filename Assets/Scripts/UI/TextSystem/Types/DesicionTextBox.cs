﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ProjectHunt;

public class DesicionTextBox : TextBoxBase
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text title;
    [SerializeField]
    private RectTransform desicionContainer;
    [SerializeField]
    private Button confirmButton;
    [SerializeField]
    private DesicionHolder desicionHolder;

    private List<DesicionHolder> activeDesicions = new List<DesicionHolder>();
    private List<IDesicion> desicions = new List<IDesicion>();

    public Text Title
    {
        get
        {
            return title;
        }
        set
        {
            title = value;
        }
    }

    public Image Image
    {
        get
        {
            return image;
        }
        set
        {
            image = value;
        }
    }

    private void OnEnable()
    {
        Data = new TextBoxData();
        Data.position = textBoxPosition.middleCenter;
    }

    public void SetDesicions(List<DesicionBase> desicions)
    {
        for(int i = 0; i<desicions.Count; i++)
        {
            var temp = Instantiate(desicionHolder.gameObject, desicionContainer).GetComponent<DesicionHolder>();
            temp.Desicion = desicions[i];
            temp.choose += Choose;
            temp.outline += HighliteDesc;
            temp.Text.text = desicions[i].Text;
            activeDesicions.Add(temp);
            temp.Init();
        }

    }

    public void HighliteDesc(DesicionHolder holder)
    {
        foreach(var item in activeDesicions)
        {
            if (holder == item)
            {
                item.GetComponent<Outline>().enabled = true;
            }
            else
                item.GetComponent<Outline>().enabled = false;
        }


    }

    public void Choose(IDesicion desicion)
    {
        confirmButton.onClick.RemoveAllListeners();
        confirmButton.onClick.AddListener(desicion.HandleDesicion);
        confirmButton.onClick.AddListener(delegate { UITextController.Instance.DestroyTextBox(this); });

    }
}
