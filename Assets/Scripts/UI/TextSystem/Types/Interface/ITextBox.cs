﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectHunt
{
    public interface ITextBox
    {
        Text TextField { get; }
        string DisplayText { get; set; }


    }
}