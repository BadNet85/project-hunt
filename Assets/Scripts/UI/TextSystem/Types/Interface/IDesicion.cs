﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDesicion
{
    string Text { get; set; }

    void HandleDesicion();
}
