﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectHunt;

public enum ExternalEventType
{
    none = 0,
    VillageDestroyed
}

public class ExternalEvent{

    [SerializeField]
    private ExternalEventType type;
    [SerializeField]
    private TextBoxBase prefab;
    [SerializeField]
    private bool fade;
    private GridCoord coord;

    public ExternalEventType Type
    {
        get
        {
            return type;
        }
    }

    public TextBoxBase Prefab
    {
        get
        {
            return prefab;
        }
    }

    public bool Fade
    {
        get
        {
            return fade;
        }
    }

    public GridCoord Coord
    {
        get
        {
            return coord;
        }
    }

    public ExternalEvent(ExternalEventType type, TextBoxBase prefab, bool fade, GridCoord coord)
    {
        this.prefab = prefab;
        this.type = type;
        this.fade = fade;
        this.coord = coord;
    }

}
