﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using ProjectHunt;

public class EventButton : MonoBehaviour {

    private int eventCount = 0;
    [SerializeField]
    private Text text;
    private bool hasFadeOut = false;

    public void Awake()
    {
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.GetComponent<Button>().onClick.AddListener(ExternalEventsController.Instance.ShowExternalEvent);
    }

    public void AddFadeOut()
    {
        if (hasFadeOut)
            return;

        hasFadeOut = true;
        gameObject.GetComponent<Button>().onClick.AddListener(ExternalEventsController.Instance.HideExternalEvent);

    }

    public void RemoveFadeOut()
    {
        hasFadeOut = false;
        gameObject.GetComponent<Button>().onClick.RemoveListener(ExternalEventsController.Instance.HideExternalEvent);
    }
    public void UpdateCounter(int count)
    {
        text.text = count.ToString();
    }

}
