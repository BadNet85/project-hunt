﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ProjectHunt;

public class ExternalEventTextBox : TextBoxBase {

    [SerializeField]
    private Button fokusButton;
    private ExternalEvent externalEvent;

    public ExternalEvent ExternalEvent
    {
        get
        {
           return externalEvent;
        }
        set
        {
            externalEvent = value;
        }
    }

    public void Init()
    {
        fokusButton.onClick.AddListener(delegate { FokusOnCoord(externalEvent.Coord); });
    }

    public void FokusOnCoord(GridCoord coord)
    {
        ExternalEventsController.Instance.ForceHideExternalEvent(this);
    }



}
