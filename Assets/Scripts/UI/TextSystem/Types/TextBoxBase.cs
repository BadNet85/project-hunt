﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectHunt
{
    public class TextBoxBase : MonoBehaviour, ITextBox
    {
        [SerializeField]
        private Text textField;
        [SerializeField]
        private Button closeButton;
        private string text;
        private float lifeTime;
        private bool hasLifetime;
        private textBoxPosition position;
        private TextBoxData data;

        public void Update()
        {
            if (lifeTime != 0)
            {
                hasLifetime = true;
                StartCoroutine(Lifespan());
            }
            if (hasLifetime && lifeTime <= 0)
            {
                UITextController.Instance.DestroyTextBox(this);
            }
        }

        IEnumerator Lifespan()
        {
            lifeTime -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        private void Awake()
        {
            if(closeButton != null)
                closeButton.onClick.AddListener(DestroyThis);
        }

        private void DestroyThis()
        {
            UITextController.Instance.DestroyTextBox(this);
        }


        public TextBoxData Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        public string DisplayText
        {

            get
            {
                return text;
            }

            set
            {
                textField.text = value;
            }
        }

        public Text TextField
        {
            get
            {
                return textField;
            }
        }

        public float LifeTime
        {
            get
            {
                return lifeTime;
            }
            set
            {
                lifeTime = value;
            }
        }


        public void SetData(TextBoxData data)
        {
            this.data = data;
            lifeTime = data.lifeTime;
            DisplayText = data.displayText;
        }
    }
}