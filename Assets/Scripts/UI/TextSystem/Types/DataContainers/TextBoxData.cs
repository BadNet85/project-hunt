﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class TextBoxData
    {
        public textBoxPosition position;
        public float lifeTime;
        public TextBoxBase prefab;
        public string displayText;

    }
}