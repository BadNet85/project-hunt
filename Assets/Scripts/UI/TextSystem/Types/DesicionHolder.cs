﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;
using ProjectHunt;

public class DesicionHolder : MonoBehaviour {

    public delegate void ChooseDelegte(DesicionBase desicion);
    public ChooseDelegte choose;

    public delegate void OutlineDelegate(DesicionHolder holder);
    public OutlineDelegate outline;
    private DesicionBase desicion;
    [SerializeField]
    private Text text;


    public DesicionBase Desicion
    {
        get
        {
            return desicion;
        }
        set
        {
            desicion = value;
        }
    }

    public Text Text
    {
        get
        {
            return text;
        }
        set
        {
            text = value;
        }
    }


    public void Init()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(delegate { choose.Invoke(desicion); });
        gameObject.GetComponent<Button>().onClick.AddListener(delegate { outline.Invoke(this); });
    }

}
