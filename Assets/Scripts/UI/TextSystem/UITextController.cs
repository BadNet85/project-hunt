﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectHunt
{
    public enum textBoxPosition
    {
        none = 0,
        middleCenter,
        middleLeft,
        middleRight,
        bottomCenter,
        bottomLeft,
        bottomRight,
        topCenter,
        topLeft,
        topRight
    }

    public class UITextController : MonoBehaviour
    {
        #region ~~VARIABLES~~
        List<TextBoxBase> activeTextBoxes = new List<TextBoxBase>();
        [SerializeField]
        RectTransform UiCanvas;
        [SerializeField]
        TextBoxBase defaultTextboxPrefab;
        List<TextBoxData> textBoxQueue = new List<TextBoxData>();
        [SerializeField]
        private float borderOffset;
        [SerializeField]
        private float FadeMultiplier;
        private bool fadingIn = false;
        private bool fadingOut = false;
        private bool locked = false;
        private Dictionary<textBoxPosition,bool> usedPositions = new Dictionary<textBoxPosition, bool>();
        #endregion

        #region ~~SINGLETON~~
        private static UITextController instance;

        public static UITextController Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<UITextController>();
                return instance;
            }
        }
        #endregion

        public void Awake()
        {
            usedPositions.Add(textBoxPosition.bottomCenter, false);
            usedPositions.Add(textBoxPosition.bottomLeft, false);
            usedPositions.Add(textBoxPosition.bottomRight, false);
            usedPositions.Add(textBoxPosition.middleCenter, false);
            usedPositions.Add(textBoxPosition.middleLeft, false);
            usedPositions.Add(textBoxPosition.middleRight, false);
            usedPositions.Add(textBoxPosition.topCenter, false);
            usedPositions.Add(textBoxPosition.topLeft, false);
            usedPositions.Add(textBoxPosition.topRight, false);
        }

        public void Update()
        {
            if (locked)
                return;
            if(textBoxQueue.Count >= 1)
            {
                for(int i = 0; i < textBoxQueue.Count; i++)
                {
                    if (!usedPositions[textBoxQueue[i].position])
                    {
                        ShowTextBox(textBoxQueue[i]);
                        textBoxQueue.Remove(textBoxQueue[i]);
                    }
                }
            }
        }


        public void QueueTextBox(textBoxPosition position, TextBoxBase prefab, string text, float lifeTime)
        {
            if (prefab == null)
                prefab = defaultTextboxPrefab;
            TextBoxData data = new TextBoxData();
            data.displayText = text;
            data.lifeTime = lifeTime;
            data.position = position;
            data.prefab = prefab;
            textBoxQueue.Add(data);

        }


        public TextBoxBase ShowTextBox(TextBoxData data)
        {

            TextBoxBase temp = Instantiate(data.prefab.gameObject, UiCanvas).GetComponent<TextBoxBase>();
            activeTextBoxes.Add(temp);
            temp.SetData(data);
            RectTransform activeRect = temp.gameObject.GetComponent<RectTransform>();
            Vector3 activeTextBoxPostion = temp.gameObject.transform.localPosition;
            Transform activeTextBoxTransform = temp.gameObject.transform;
            usedPositions[data.position] = true;

            switch (data.position)
            {
                case (textBoxPosition.middleCenter):
                    {
                        activeRect.pivot = new Vector2(0.5f, 0.5f);
                        activeRect.anchorMax = new Vector2(0.5f, 0.5f);
                        activeRect.anchorMin = new Vector2(0.5f, 0.5f);
                        return temp;
                    }
                case (textBoxPosition.middleLeft):
                    {
                        activeRect.pivot = new Vector2(0f, 0.5f);
                        activeRect.anchorMax = new Vector2(0f, 0.5f);
                        activeRect.anchorMin = new Vector2(0f, 0.5f);
                        return temp;
                    }
                case (textBoxPosition.middleRight):
                    {
                        activeRect.pivot = new Vector2(1f, 0.5f);
                        activeRect.anchorMax = new Vector2(1f, 0.5f);
                        activeRect.anchorMin = new Vector2(1f, 0.5f);
                        return temp;
                    }
                case (textBoxPosition.topRight):
                    {
                        activeRect.pivot = new Vector2(1f, 1f);
                        activeRect.anchorMax = new Vector2(1f, 1f);
                        activeRect.anchorMin = new Vector2(1f, 1f);
                        return temp;
                    }
                case (textBoxPosition.topLeft):
                    {
                        activeRect.pivot = new Vector2(0f, 1f);
                        activeRect.anchorMax = new Vector2(0f, 1f);
                        activeRect.anchorMin = new Vector2(0f, 1f);
                        return temp;
                    }
                case (textBoxPosition.topCenter):
                    {
                        activeRect.pivot = new Vector2(0.5f, 1f);
                        activeRect.anchorMax = new Vector2(0.5f, 1f);
                        activeRect.anchorMin = new Vector2(0.5f, 1f);
                        return temp;
                    }
                case (textBoxPosition.bottomCenter):
                    {
                        activeRect.pivot = new Vector2(0.5f, 0f);
                        activeRect.anchorMax = new Vector2(0.5f, 0f);
                        activeRect.anchorMin = new Vector2(0.5f, 0f);
                        return temp;
                    }
                case (textBoxPosition.bottomRight):
                    {
                        activeRect.pivot = new Vector2(1f, 0f);
                        activeRect.anchorMax = new Vector2(1f, 0f);
                        activeRect.anchorMin = new Vector2(1f, 0f);
                        return temp;
                    }
                case (textBoxPosition.bottomLeft):
                    {
                        activeRect.pivot = new Vector2(0f, 0f);
                        activeRect.anchorMax = new Vector2(0f, 0f);
                        activeRect.anchorMin = new Vector2(0f, 0f);
                        return temp;
                    }
            }
            return temp;
        }

        public void ShowDesicionBox(DesicionTextBox prefab, string text, float lifetime, string title, Sprite flavourImage, List<DesicionBase> desicions, Group group)
        {
            DesicionTextBox temp = Instantiate(prefab.gameObject, UiCanvas).GetComponent<DesicionTextBox>();
            activeTextBoxes.Add(temp);
            temp.TextField.text = text;
            temp.Title.text = title;
            temp.Image.sprite = flavourImage;
            foreach(DesicionBase desicion in desicions)
            {
                desicion.CallGroup = group;
            }
            usedPositions[temp.Data.position] = true;
            temp.SetDesicions(desicions);
        }

        public ExternalEventTextBox ShowExternalEventBox(ExternalEvent extEvent)
        {
            if (extEvent.Type == ExternalEventType.VillageDestroyed)
                extEvent.Prefab.TextField.text = "Das Dorf " + extEvent.Coord.ToString() + " wurde zerstört!";
            var temp = Instantiate(extEvent.Prefab, UiCanvas).GetComponent<ExternalEventTextBox>();




            var activeRect = temp.gameObject.GetComponent<RectTransform>();
            activeRect.pivot = new Vector2(0.5f, 1f);
            activeRect.anchorMax = new Vector2(0.5f, 1f);
            activeRect.anchorMin = new Vector2(0.5f, 1f);
            if (extEvent.Fade)
            {
                activeRect.Translate(0, activeRect.sizeDelta.y + 5, 0);
                StartCoroutine(FadeIn(temp));
            }
            activeTextBoxes.Add(temp);
            return temp;
        }
        public void DestroyTextBox(TextBoxBase textBox)
        {
            activeTextBoxes.Remove(textBox);
            usedPositions[textBox.Data.position] = false;
            DestroyObject(textBox.gameObject);
        }


        public IEnumerator FadeIn(TextBoxBase textBox)
        {
            yield return new WaitUntil(() => fadingOut != true);
            fadingIn = true;
            var temp = textBox.gameObject.GetComponent<RectTransform>();
            temp.Translate(0, -Time.deltaTime * FadeMultiplier, 0);
            yield return new WaitForEndOfFrame();
            if (!(RectTransformExt.GetWorldRect(temp, Vector2.one).yMax <= RectTransformExt.GetWorldRect(UiCanvas, Vector2.one).yMax))
            {
                StartCoroutine(FadeIn(textBox));
            }
            else
                fadingIn = false;
        }


        public void HideExternalEventTextBox(TextBoxBase textBox)
        {
            StartCoroutine(FadeOut(textBox));
        }

        public IEnumerator FadeOut(TextBoxBase textBox)
        {
            fadingOut = true;
            var temp = textBox.gameObject.GetComponent<RectTransform>();
            temp.Translate(0, Time.deltaTime * FadeMultiplier, 0);
            yield return new WaitForEndOfFrame();
            if (!(RectTransformExt.GetWorldRect(temp, Vector2.one).yMin > RectTransformExt.GetWorldRect(UiCanvas, Vector2.one).yMax))
            {
                StartCoroutine(FadeOut(textBox));
            }
            else
            {
                fadingOut = false;
                activeTextBoxes.Remove(textBox);
                Destroy(textBox.gameObject);
            }
        }

        public void LockSystem(bool lockIt)
        {
            locked = lockIt;
        }

        public void SetUiActive(bool active)
        {
            UiCanvas.gameObject.SetActive(active);
        }
    }
}