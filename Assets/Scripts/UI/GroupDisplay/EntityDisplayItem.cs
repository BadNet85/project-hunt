﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ProjectHunt
{
    public class EntityDisplayItem : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        #region Data
        [SerializeField]
        private GroupDisplayItem gdi;
        private Transform tempParent;
        private Image image;
        private Entity entity;
        #endregion

        #region Properties

        public Entity Entity
        {
            get { return entity; }
            set { entity = value; }
        }

        public GroupDisplayItem GroupDisplayItem
        {
            get { return gdi; }
            set { gdi = value; }
        }

        public Sprite Portrait
        {
            get { return image.sprite; }
            set
            {
                image.sprite = value;
                image.preserveAspect = true;
            }
        }

        #endregion

        #region Events
        public delegate void EntityDragged();
        public static event EntityDragged OnEntityDragged;
        public delegate void EntityDropped(GroupDisplayItem fromDisplay, EntityDisplayItem entityDisplayItem, Vector2 position);
        public static event EntityDropped OnEntityDropped;

        private void Awake()
        {
            image = GetComponent<Image>();
            tempParent = gdi.transform.parent.parent.parent;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (GroupDisplay.dragEnabled)
            {
                transform.SetParent(tempParent);
                image.raycastTarget = false;
                OnEntityDragged?.Invoke();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (GroupDisplay.dragEnabled)
            {
                transform.position = Input.mousePosition;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (GroupDisplay.dragEnabled)
            {
                transform.SetParent(gdi.EntityListTransform);
                transform.SetAsFirstSibling();
                image.raycastTarget = true;
                OnEntityDropped?.Invoke(gdi, this, Input.mousePosition);
            }
        }
        #endregion
    }
}
