﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ProjectHunt
{
    public class GroupDisplayItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        #region Data
        [SerializeField]
        private EntityDisplayItem entityDisplayItemPrefab;
        [SerializeField]
        private RectTransform sidedrop;
        [SerializeField]
        private Transform entityListTransform;
        private List<EntityDisplayItem> entityDisplayItemList;
        [SerializeField]
        private Image item, sidebar;
        private Vector2 min;
        [SerializeField]
        private Sprite normal, highlight;

        [SerializeField]
        float slidingSpeed = 3f;
        private Text apText;
        private GroupDisplay groupDisplay;
        private bool coroutineRunning = false;
        #endregion
    
        #region Properties
        public RectTransform RectTransform
        {
            get;
            private set;
        }

        public RectTransform SliderTransfrom
        {
            get { return sidedrop; }
            private set { sidedrop = value; }
        }

        public Transform EntityListTransform
        {
            get { return entityListTransform; }
            private set { entityListTransform = value; }
        }
    
        public uint GroupID
        {
            get;
            private set;
        }

        public uint MemberID
        {
            get;
            private set;
        }

        public int Size
        {
            get { return entityDisplayItemList.Count; }
        }
        #endregion
    
        #region Events
        protected void Awake()
        {
            RectTransform = GetComponent<RectTransform>();
            min = sidedrop.sizeDelta;
            apText = GetComponentInChildren<Text>();
            entityDisplayItemList = new List<EntityDisplayItem>();
        }
        #endregion
    
        #region Methods
        public void Init(Group group, Color color, GroupDisplay groupDisplay)
        {
            GroupID = group.GroupID;
            MemberID = group.MemberID;
            item.color = color;
            sidebar.color = color;
            apText.text = group.CurrentAP.ToString();
            group.OnApChanged += UpdateAP;
            this.groupDisplay = groupDisplay;

            var entities = group.GetMembers();
            foreach (Entity entity in entities)
            {
                CreateNewEntityDisplayItem(entity);
            }
        }

        public void SetHighlight(bool highlighted)
        {
            if (highlighted)
                item.sprite = highlight;
            else
                item.sprite = normal;
        }

        public void CreateNewEntityDisplayItem(Entity entity)
        {
            EntityDisplayItem item = Instantiate(entityDisplayItemPrefab, entityListTransform);
            item.gameObject.SetActive(true);
            item.Entity = entity;
            if(entity.portrait != null)
                item.Portrait = entity.portrait;
            AddEntityDisplayItem(item);
        }

        public void AddEntityDisplayItem(EntityDisplayItem entityDisplayItem)
        {
            entityDisplayItemList.Add(entityDisplayItem);
            entityDisplayItem.transform.SetParent(entityListTransform);
            entityDisplayItem.transform.SetAsFirstSibling();
            entityDisplayItem.GroupDisplayItem = this;
            StartCoroutine(Slide(false));
        }

        public bool ContainsEntity(Entity entity)
        {
            foreach (var item in entityDisplayItemList)
            {
                if (item.Entity == entity)
                    return true;
            }
            return false;
        }

        public void RemoveEntitiyDisplayItem(EntityDisplayItem entityDisplayItem)
        {
            entityDisplayItemList.Remove(entityDisplayItem);
        }

        public EntityDisplayItem GetEntityDisplayItem(Entity entity)
        {
            foreach (var item in entityDisplayItemList)
            {
                if (item.Entity == entity)
                    return item;
            }
            return null;
        }

        public List<EntityDisplayItem> GetEntityDisplayItemList()
        {
            return entityDisplayItemList;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (coroutineRunning)
                StopAllCoroutines();
            StartCoroutine(Slide(true));
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (coroutineRunning)
                StopAllCoroutines();
            StartCoroutine(Slide(false));
        }

        public void ItemClicked()
        {
            groupDisplay.ItemClicked(GroupID, MemberID);
        }

        public void UpdateSlideWithDelay(bool maximize = false, float delay = 0.5f)
        {
            StartCoroutine(SlideUpdate(maximize, delay));
        }

        private IEnumerator SlideUpdate(bool maximize, float delay)
        {
            yield return new WaitForSeconds(delay);
            StartCoroutine(Slide(maximize));
        }
    
        private IEnumerator Slide(bool maximize)
        {
            coroutineRunning = true;
            float step = 0;
            float anchoredPositionX = sidedrop.anchoredPosition.x;
            while (maximize ? 
                sidedrop.anchoredPosition.x > 0 : 
                sidedrop.anchoredPosition.x < sidedrop.sizeDelta.x)
            {
                step += slidingSpeed * Time.deltaTime;
                sidedrop.anchoredPosition = maximize ? 
                    new Vector2(Mathf.Lerp(anchoredPositionX, 0, step), sidedrop.anchoredPosition.y) : 
                    new Vector2(Mathf.Lerp(anchoredPositionX, sidedrop.sizeDelta.x, step), sidedrop.anchoredPosition.y);

                //Scale icon
                //if (maximize)
                //    this.rectTransform.sizeDelta = Vector2.Lerp(myInitialSizeDelta, myInitialSizeDelta * 1.2f, step);
                //else
                //    this.rectTransform.sizeDelta = Vector2.Lerp(myInitialSizeDelta * 1.2f, myInitialSizeDelta, step);

                yield return null;
            }
            maximize = maximize ? false : true;
            coroutineRunning = false;
        }

        private void UpdateAP(int ap)
        {
            apText.text = ap.ToString();
            if(ap == 0)
            {
                apText.color = Color.gray;
            }
            else
            {
                apText.color = Color.white;
            }
        }


        #endregion
    }
}
