﻿sampler2D _GridTileData;
float4 _GridTileData_TexelSize;

float4 GetCellData(float2 cellDataCoordinates) {
	float2 uv = cellDataCoordinates + 0.5;
	uv.x *= _GridTileData_TexelSize.x;
	uv.y *= _GridTileData_TexelSize.y;
	return tex2Dlod(_GridTileData, float4(uv, 0, 0));
}